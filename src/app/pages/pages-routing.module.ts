import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PagesComponent } from "./pages.component";
import { AuthGuard } from "../_services/auth.guard";
import { ListUsersComponent } from "./admin/users/list-users/list-users.component";
import { MisProyectosComponent } from './proyectos/mis-proyectos/mis-proyectos.component';
import { AgregarProyectoComponent } from './admin/proyectos/agregar-proyecto/agregar-proyecto.component';
import { ListRolesComponent } from './admin/roles/list-roles/list-roles.component';
import { PermisosRolSistemaComponent } from './admin/roles/permisos-rol-sistema/permisos-rol-sistema.component';

const routes: Routes = [
  {
    path: "",
    component: PagesComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "",
        redirectTo: "mis-proyectos",
        pathMatch: "full"
      },
      {
        path: "mis-proyectos",
        component: MisProyectosComponent
      },
      {
        path: "mis-proyectos/:id",
        loadChildren: () => import('./proyectos/mis-proyectos-detail/mis-proyectos-detail.module').then(m => m.MisProyectosDetailModule)
      },
      {
        path: "administracion/proyectos/agregar",
        component: AgregarProyectoComponent,
        data: { permiso: "proyectos_add" }
      },
      {
        path: "administracion/usuarios",
        component: ListUsersComponent,
        data: { permiso: "usuarios" }
      },
      {
        path: "administracion/roles-sistema",
        component: ListRolesComponent,
        data: { permiso: "roles" }
      },
      {
        path: "administracion/roles-sistema/:id/gestionar",
        component: PermisosRolSistemaComponent,
        data: { permiso: "roles" }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {}
