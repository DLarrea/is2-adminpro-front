import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoSolicitudesComponent } from './proyecto-solicitudes.component';

describe('ProyectoSolicitudesComponent', () => {
  let component: ProyectoSolicitudesComponent;
  let fixture: ComponentFixture<ProyectoSolicitudesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoSolicitudesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoSolicitudesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
