import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/_models/user';
import { RolProyectoService } from 'src/app/_services/rolProyecto.service';
import { ToastrService } from 'ngx-toastr';
import { SolicitudService } from 'src/app/_services/solicitud.service';
import { DatePipe } from '@angular/common';
import Hashids from 'hashids'
import { AuthenticationService } from 'src/app/_services/authentication.service';

declare var $:any;
@Component({
  selector: 'app-proyecto-solicitudes',
  templateUrl: './proyecto-solicitudes.component.html',
  styleUrls: ['./proyecto-solicitudes.component.css']
})
export class ProyectoSolicitudesComponent implements OnInit {

  proyectoId = null;
  hashids = new Hashids('adminpro', 5)
  constructor(
    private apiSolicitud: SolicitudService,
    private route: ActivatedRoute,
    private apiRol: RolProyectoService,
    private toastr: ToastrService,
    private datePipe: DatePipe,
    private apiAuth: AuthenticationService
  ) {
    this.route.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });
    this.user = this.apiAuth.currentUserValue;
  }

  user = null;
  loading = true;
  solicitudes = [];
  miembros = [];
  estados = {
    "PE": "PENDIENTE",
    "SAP": "SOLICITUD APROBADA",
    "SRE": "SOLICITUD RECHAZADA"
  }

  estados_badge = {
    "PE": "badge-warning",
    "SAP": "badge-success",
    "SRE": "badge-danger"
  }

  ngOnInit(): void {
    this.getSolicitudes();
  }

  getSolicitudes() {
    this.loading = true;
    this.apiSolicitud.get(this.proyectoId).subscribe(data => {
      this.miembros = data.miembros;
      this.solicitudes = data.datos;
      this.verificar_puede_votar();
      for(let i=0; i< this.solicitudes.length;i++){
        this.solicitudes[i].solicitud.fecha_creacion = this.datePipe.transform(this.solicitudes[i].solicitud.fecha_creacion,"dd/MM/yyy HH:mm:ss");
        this.solicitudes[i].solicitud.item["codigo"] = this.hashids.encode(this.solicitudes[i].solicitud.item.id);
        for(let j=0; j<this.solicitudes[i].items_afectados.length; j++){
          let it = this.solicitudes[i].items_afectados[j];
          it["codigo"] = this.hashids.encode(it.id);
        }
      }
      this.loading = false;
    },
      error => {
        this.toastr.error("Error al recuperar solicitudes", "Usuarios");
      });
  }

  solicitudDetalles: any = {};
  openDetalles(target){
    this.solicitudDetalles = target;
    this.votantes();
  }

  puede_votar = false;
  id_miembro = null;
  verificar_puede_votar(){
    for(let i=0;i<this.miembros.length;i++){
      let m = this.miembros[i];
      if(m.miembro.id == Number(this.user.id) && m.activo){
        this.id_miembro = m.id;
        this.puede_votar = true;
      }
    }
  }

  votos_hechos = [];
  votantes(){
    this.votos_hechos = [];
    for(let i=0; i<this.solicitudDetalles.votos_usuarios.length;i++){
      let v = this.solicitudDetalles.votos_usuarios[i];
      let miembro_id = v.miembro.id;
      for(let j=0;j<this.miembros.length;j++){
        let m = this.miembros[j];
        if(Number(miembro_id) ==  m.id){
          this.votos_hechos.push({
            voto: v.voto,
            mensaje: v.voto? `El usuario ${m.miembro.email} votó a favor`: `El usuario ${m.miembro.email} votó en contra`
          });
        }
      }
    }
  }

  votar(voto){
    let body = {
      solicitud: this.solicitudDetalles.solicitud.id,
      miembro: this.id_miembro,
      voto: voto
    }

    this.apiSolicitud.put(body).subscribe(
      data => {
        this.toastr.success("Su voto fue registrado");
        $("#votacionModal").modal("hide");
        this.getSolicitudes();
      }
    )
  }
}
