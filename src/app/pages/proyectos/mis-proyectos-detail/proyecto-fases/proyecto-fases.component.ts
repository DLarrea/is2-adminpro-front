import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FaseService } from 'src/app/_services/fase.service';
import { ToastrService } from 'ngx-toastr';
import { Fase } from 'src/app/_models/fase';
import { PermService } from 'src/app/_services/perm.service';
import { NgxPermissionsService } from 'ngx-permissions';
declare var $: any;
@Component({
  selector: 'app-proyecto-fases',
  templateUrl: './proyecto-fases.component.html',
  styleUrls: ['./proyecto-fases.component.css']
})
export class ProyectoFasesComponent implements OnInit {

  proyectoId = null;
  constructor(private route: ActivatedRoute, private apiFase: FaseService, 
    private toastr: ToastrService, private permService: PermService, private ngxP: NgxPermissionsService) {
    this.route.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });
  }
  p: number = 1;
  agregarFase = true;

  faseAgregar: Fase = new Fase();
  faseEditar: Fase = new Fase();

  estadosFase = {
    "CR": "Creada",
    "AB": "Abierta",
    "CE": "Cerrada"
  }
  async ngOnInit() {
    let u = JSON.parse(sessionStorage.getItem("currentUser"))
    await this.permService.getPermisosProyecto(u.id, this.proyectoId)
    .toPromise()
    .then(
      res => {
        let perms = [];
        for( let i=0; i< res.length; i++ ){
          perms.push(res[i].permiso);
        }
        this.ngxP.loadPermissions(perms);
        sessionStorage.setItem('pp',JSON.stringify(res))
      }
    )
    this.getFases();
  }

  fases: any[] = [];
  getFases() {
    this.apiFase.get(this.proyectoId).subscribe(data => {
      this.fases = data;
      this.fases.forEach((el) => {
        if (el.estado_fase == "AB" || el.estado_fase == "CE") {
          this.agregarFase = false;
        }
      });
      console.log(this.fases);
    },
      error => {
        this.toastr.error("Error al recuperar fases", "Fases");
      });
  }

  postFase() {
    let isIn = false;
    for (let i = 0; i < this.fases.length; i++) {
      let f = this.fases[i];
      if (f.orden != null) {
        if (f.orden == this.faseAgregar.orden) {
          isIn = true;
          break;
        }
      }
    }
    if (isIn) {
      this.toastr.error("El orden ya se encuentra asignado");
      return;
    }

    if (this.faseAgregar.nombre != null && this.faseAgregar.descripcion != null &&
      this.faseAgregar.nombre != "" && this.faseAgregar.descripcion != "") {
      this.faseAgregar.proyecto = this.proyectoId;
      this.faseAgregar.estado_fase = "CR";
      this.apiFase.post(this.faseAgregar).subscribe(
        data => {
          this.faseAgregar = new Fase();
          this.getFases();
          $("#agregarFase").modal("hide");
          this.toastr.success("Fase agregada");
        }
      )
    }else{
      this.toastr.error("Complete los campos");
    }
  }

  putFase() {
    if (this.faseEditar.nombre != null && this.faseEditar.descripcion != null &&
      this.faseEditar.nombre != "" && this.faseEditar.descripcion != "") {
      this.apiFase.put(this.faseEditar).subscribe(
        data => {
          this.faseEditar = new Fase();
          this.getFases();
          $("#editarFase").modal("hide");
          this.toastr.success("Fase editada");
        }
      )
    }else{
      this.toastr.error("Complete los campos");
    }
  }

  deleteFase(id){
    this.apiFase.delete(id).subscribe(
      data => {
        this.toastr.success("Fase eliminada");
        this.getFases();
      }
    )
  }

  iniciarFase(target:Fase) {
    target.estado_fase = "AB";
    this.apiFase.put(target).subscribe(
      data => {
        this.getFases();
        this.toastr.success("Fase iniciada");
      }
    )
  }

  faseCerrarObject = null;
  openCerrarFase(target){
    this.faseCerrarObject = Object.assign({}, target);
  }

  cerrarFase() {
    let f:Fase = Object.assign({}, this.faseCerrarObject);
    f.estado_fase = "CE";
    this.apiFase.put(f).subscribe(
      data => {
        this.getFases();
        this.toastr.success("Fase cerrada");
      },
      error => {
        this.toastr.error(error.error.mensaje);
      }
    )
  }
}
