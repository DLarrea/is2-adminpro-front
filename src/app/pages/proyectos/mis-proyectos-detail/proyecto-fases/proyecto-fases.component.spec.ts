import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoFasesComponent } from './proyecto-fases.component';

describe('ProyectoFasesComponent', () => {
  let component: ProyectoFasesComponent;
  let fixture: ComponentFixture<ProyectoFasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoFasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoFasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
