import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { PermService } from 'src/app/_services/perm.service';
import { Node, Edge, ClusterNode } from '@swimlane/ngx-graph';
import { RelacionService } from 'src/app/_services/relaciones.service';
import { ComiteService } from 'src/app/_services/comite.service';

@Component({
  selector: 'app-proyecto-overview',
  templateUrl: './proyecto-overview.component.html',
  styleUrls: ['./proyecto-overview.component.css']
})
export class ProyectoOverviewComponent implements OnInit {

  proyectoId = null;
  constructor(private route: ActivatedRoute, private permService: PermService, 
    private ngxP: NgxPermissionsService, private apiRelacion: RelacionService, private apiComite: ComiteService) {
    this.route.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });
  }

  u = null;
  async ngOnInit() {
    this.u = JSON.parse(sessionStorage.getItem("currentUser"));
    this.conexo();
    this.miembro();
    await this.permService.getPermisosProyecto(this.u.id, this.proyectoId)
    .toPromise()
    .then(
      res => {
        let perms = [];
        for( let i=0; i< res.length; i++ ){
          perms.push(res[i].permiso);
        }
        this.ngxP.loadPermissions(perms);
        sessionStorage.setItem('pp',JSON.stringify(res))
      }
    )
  }

  items = [
    {
      icon: "fas fa-layer-group fa-2x text-primary",
      nombre: "Fases",
      route: 'fases',
      p: null,
      show: true
    },
    {
      icon: "fas fa-project-diagram fa-2x text-primary",
      nombre: "Relaciones y trazabilidad",
      route: 'relaciones',
      p: null,
      show: true
    },
    {
      icon: "fas fa-edit fa-2x text-primary",
      nombre: "Solicitudes",
      route: 'solicitudes',
      p: null,
      show: false
    },
    {
      icon: "fas fa-exclamation fa-2x text-primary",
      nombre: "Ítems en revisión",
      route: 'items-revisar',
      p: 'p_revisar'
    },
    {
      icon: "fas fa-users fa-2x text-primary",
      nombre: "Usuarios",
      route: 'usuarios',
      p: 'p_usuarios'
    },
    {
      icon: "fas fa-users-cog fa-2x text-primary",
      nombre: "Comité",
      route: 'comite',
      p: 'p_comite'
    },
    {
      icon: "fas fa-cogs fa-2x text-primary",
      nombre: "Roles",
      route: 'roles',
      p: 'p_roles'
    },
    {
      icon: "fas fa-clipboard-list fa-2x text-primary",
      nombre: "Tipos de item",
      route: 'tipos-item',
      p: 'p_tipos_item'
    },
    {
      icon: "fas fa-list fa-2x text-primary",
      nombre: "Atributos",
      route: 'atributos',
      p: 'p_atributos'
    },
    {
      icon: "fas fa-bell fa-2x text-primary",
      nombre: "Notificaciones",
      route: 'notificaciones',
      p: null,
      show: true
    }
  ]

  isconexo = null;
  conexo(){
    this.apiRelacion.esConexo(this.proyectoId).subscribe(
      data => {
        this.isconexo = data;
        console.log(data);
      }
    )
  }

  isMiembro = false;
  miembro(){
    this.apiComite.isMiembro(this.proyectoId, this.u.id).subscribe(
      data => {
        this.isMiembro = data.miembro;
        this.items[2].show = this.isMiembro;
      }
    )
  }
}
