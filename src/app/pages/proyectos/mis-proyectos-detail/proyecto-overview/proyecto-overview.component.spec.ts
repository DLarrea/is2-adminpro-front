import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoOverviewComponent } from './proyecto-overview.component';

describe('ProyectoOverviewComponent', () => {
  let component: ProyectoOverviewComponent;
  let fixture: ComponentFixture<ProyectoOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
