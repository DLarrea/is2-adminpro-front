import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoUsuariosComponent } from './proyecto-usuarios.component';

describe('ProyectoUsuariosComponent', () => {
  let component: ProyectoUsuariosComponent;
  let fixture: ComponentFixture<ProyectoUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
