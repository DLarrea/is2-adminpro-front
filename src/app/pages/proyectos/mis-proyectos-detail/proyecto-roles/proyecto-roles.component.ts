import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/_models/user';
import { RolProyectoService } from 'src/app/_services/rolProyecto.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-proyecto-roles',
  templateUrl: './proyecto-roles.component.html',
  styleUrls: ['./proyecto-roles.component.css']
})
export class ProyectoRolesComponent implements OnInit {

  proyectoId = null;
  constructor(
    private route: ActivatedRoute,
    private apiRol: RolProyectoService,
    private toastr: ToastrService
  ) {
    this.route.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });
  }

  loading = true;
  roles = [];

  //Variables de modal agregar rol
  user = new User();
  loadingSave = false;

  //Variable de paginado
  p: number = 1;

  rol: string = "";

  ngOnInit(): void {
    this.getRoles();
    // this.permissionsService.loadPermissions(this.perms);
  }

  getRoles() {
    this.loading = true;
    this.apiRol.get(this.proyectoId).subscribe(data => {
      this.roles = data;
      this.loading = false;
    },
      error => {
        this.toastr.error("Error al recuperar roles", "Usuarios");
      });
  }

  postRol() {
    if (this.rol != "") {
      this.loadingSave = true;
      let body = {
        nombre: this.rol,
        proyecto: this.proyectoId
      }
      this.apiRol.post(body).subscribe(
        data => {
          this.toastr.success("Rol agregado");
          this.rol = "";
          this.loadingSave = false;
          this.getRoles();
        },
        error => {
          this.toastr.error("Error al guardar rol");
          this.loadingSave = false;
        }
      )
    }
  }

  eliminarRol(id) {
    this.apiRol.delete(id).subscribe(
      data => {
        this.getRoles();
      },
      error => {
        let err = error.error;

        if (err.error) {
          if (err.hasOwnProperty("mensaje")) {
            this.toastr.error(err.mensaje);
          } else {
            this.toastr.error("Error al eliminar rol");
          }
        } else {
          this.toastr.error("Error al eliminar rol");
        }
      }
    )
  }

  rolPermisosModalRestul = {
    rol: {
      id: null,
      nombre: "",
      proyecto: null
    },
    permisos_asignados: [],
    permisos_disponibles: []
  }
  rolPermisosModal = null;
  obtenerPermisos(rol) {
    this.rolPermisosModal = rol;
    this.loading = true;
    this.apiRol.getPermisos(rol.id).subscribe(
      data => {
        this.rolPermisosModalRestul = data;
        this.rolPermisosModalRestul.permisos_disponibles = this.rolPermisosModalRestul.permisos_disponibles.filter((it)=>{
          return it.tipo_permiso == "P";
        }); 
        this.loading = false;
      }
    )
  }

  asignarPermiso(idPermiso) {
    this.apiRol.asignarPermiso(this.rolPermisosModal.id, idPermiso).subscribe(
      data => { this.obtenerPermisos(this.rolPermisosModal) },
      error => { }
    )
  }

  desasignarPermiso(idPermiso) {
    this.apiRol.desasignarPermiso(this.rolPermisosModal.id, idPermiso).subscribe(
      data => { this.obtenerPermisos(this.rolPermisosModal) },
      error => { }
    )
  }

}
