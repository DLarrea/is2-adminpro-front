import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MisProyectosDetailComponent } from './mis-proyectos-detail.component';
import { ProyectoFasesComponent } from './proyecto-fases/proyecto-fases.component';
import { ProyectoUsuariosComponent } from './proyecto-usuarios/proyecto-usuarios.component';
import { ProyectoRolesComponent } from './proyecto-roles/proyecto-roles.component';
import { ProyectoOverviewComponent } from './proyecto-overview/proyecto-overview.component';
import { ProyectoTiposItemComponent } from './proyecto-tipos-item/proyecto-tipos-item.component';
import { ProyectoAtributosComponent } from './proyecto-atributos/proyecto-atributos.component';
import { ProyectoComiteComponent } from './proyecto-comite/proyecto-comite.component';
import { ProyectoFasesTiposItemComponent } from './proyecto-fases-tipos-item/proyecto-fases-tipos-item.component';
import { ProyectoFasesUsuariosComponent } from './proyecto-fases-usuarios/proyecto-fases-usuarios.component';
import { ProyectoNotificacionesComponent } from './proyecto-notificaciones/proyecto-notificaciones.component';
import { ProyectoSolicitudesComponent } from './proyecto-solicitudes/proyecto-solicitudes.component';
import { ProyectoRelacionesComponent } from './proyecto-relaciones/proyecto-relaciones.component';
import { ProyectoItemsRevisarComponent } from './proyecto-items-revisar/proyecto-items-revisar.component';


const routes: Routes = [
  {
    path: "",
    component: MisProyectosDetailComponent,
    children: [
      {
        path: "",
        redirectTo: "overview",
        pathMatch: "full"
      },
      {
        path: "overview",
        component: ProyectoOverviewComponent
      },
      {
        path: "fases",
        component: ProyectoFasesComponent
      },
      {
        path: "fases/:idFase/tipos-item",
        component: ProyectoFasesTiposItemComponent
      },
      {
        path: "fases/:idFase/usuarios",
        component: ProyectoFasesUsuariosComponent
      },
      {
        path: "fases/:idFase/items",
        loadChildren: () => import('./proyecto-fases-items/proyecto-fases-items.module').then(m => m.ProyectoFasesItemsModule)
      },
      {
        path: "usuarios",
        component: ProyectoUsuariosComponent
      },
      {
        path: "roles",
        component: ProyectoRolesComponent
      },
      {
        path: "tipos-item",
        component: ProyectoTiposItemComponent
      },
      {
        path: "atributos",
        component: ProyectoAtributosComponent
      },
      {
        path: "comite",
        component: ProyectoComiteComponent
      },
      {
        path: "solicitudes",
        component: ProyectoSolicitudesComponent
      },
      {
        path: "notificaciones",
        component: ProyectoNotificacionesComponent
      },
      {
        path: "relaciones",
        component: ProyectoRelacionesComponent
      },
      {
        path: "items-revisar",
        component: ProyectoItemsRevisarComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MisProyectosDetailRoutingModule { }
