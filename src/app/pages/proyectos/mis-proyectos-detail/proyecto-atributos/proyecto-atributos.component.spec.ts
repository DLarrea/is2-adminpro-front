import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoAtributosComponent } from './proyecto-atributos.component';

describe('ProyectoAtributosComponent', () => {
  let component: ProyectoAtributosComponent;
  let fixture: ComponentFixture<ProyectoAtributosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoAtributosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoAtributosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
