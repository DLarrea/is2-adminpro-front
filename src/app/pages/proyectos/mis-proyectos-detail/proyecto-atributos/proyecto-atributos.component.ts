import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AtributoService } from 'src/app/_services/atributo.service';

@Component({
  selector: 'app-proyecto-atributos',
  templateUrl: './proyecto-atributos.component.html',
  styleUrls: ['./proyecto-atributos.component.css']
})
export class ProyectoAtributosComponent implements OnInit {

  proyectoId = null;
  constructor(private route: ActivatedRoute,
    private toastr: ToastrService,
    private apiAtributo: AtributoService) {
    this.route.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });
  }

  //Variables de pantalla principal
  loading = true;
  atributos = [];

  atributo = {
    nombre: null,
    type: null
  }

  loadingSave = false;

  //Variable de paginado
  p: number = 1;

  rol: string = "";
  agregar = false;

  ngOnInit() {
    this.getAtributos();
  }

  getAtributos() {
    this.loading = true;
    this.apiAtributo.get().subscribe(data => {
      this.atributos = data;

      for (let i = 0; i < this.atributos.length; i++) {
        if (this.atributos[i].type == "A") {
          this.atributos[i].type = "ARCHIVO"
        } else if (this.atributos[i].type == "T") {
          this.atributos[i].type = "TEXTO"
        } else {
          this.atributos[i].type = "NUMÉRICO"
        }
      }
      this.loading = false;
    },
      error => {
        this.toastr.error("Error al recuperar atributos");
      });
  }

  post() {
    if (this.atributo.nombre != null && this.atributo.type != null) {
      this.loadingSave = true;
      const body = {
        nombre: this.atributo.nombre,
        type: this.atributo.type,
      }
      this.apiAtributo.post(body).subscribe(
        data => {
          this.toastr.success("Atributo creado");
          this.loadingSave = false;
          this.getAtributos();
          this.atributo.nombre = null;
          this.atributo.type = null;
        },
        error => {
          this.toastr.error("Error al crear atributo");
          this.loadingSave = false;
        }
      )
    } else {
      this.toastr.error("Complete los campos");
    }
  }

  delete(id) {
    this.apiAtributo.delete(id).subscribe(
      data => {
        this.toastr.success("Atributo eliminado");
        this.getAtributos();
      },
      error => {
        this.toastr.error("Atributo asociado a tipo de item");
      }
    )
  }


}
