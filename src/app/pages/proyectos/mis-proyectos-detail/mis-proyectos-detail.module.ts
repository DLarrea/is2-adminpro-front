import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from "@angular/common";
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastrModule } from "ngx-toastr";
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule } from "@angular/forms";

import { MisProyectosDetailRoutingModule } from './mis-proyectos-detail-routing.module';
import { MisProyectosDetailComponent } from './mis-proyectos-detail.component';
import { ProyectoFasesComponent } from './proyecto-fases/proyecto-fases.component';
import { ProyectoUsuariosComponent } from './proyecto-usuarios/proyecto-usuarios.component';
import { ProyectoRolesComponent } from './proyecto-roles/proyecto-roles.component';
import { ProyectoOverviewComponent } from './proyecto-overview/proyecto-overview.component';
import { ProyectoTiposItemComponent } from './proyecto-tipos-item/proyecto-tipos-item.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPermissionsModule } from 'ngx-permissions';
import { environment } from 'src/environments/environment';
import { ProyectoComiteComponent } from './proyecto-comite/proyecto-comite.component';
import { ProyectoAtributosComponent } from './proyecto-atributos/proyecto-atributos.component';
import { ProyectoFasesTiposItemComponent } from './proyecto-fases-tipos-item/proyecto-fases-tipos-item.component';
import { ProyectoFasesUsuariosComponent } from './proyecto-fases-usuarios/proyecto-fases-usuarios.component';
import { ProyectoNotificacionesComponent } from './proyecto-notificaciones/proyecto-notificaciones.component';
import { ProyectoSolicitudesComponent } from './proyecto-solicitudes/proyecto-solicitudes.component';
import { NgxGraphModule } from '@swimlane/ngx-graph';
import { ProyectoRelacionesComponent } from './proyecto-relaciones/proyecto-relaciones.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ProyectoItemsRevisarComponent } from './proyecto-items-revisar/proyecto-items-revisar.component';

@NgModule({
  declarations: [
    MisProyectosDetailComponent,
    ProyectoFasesComponent,
    ProyectoUsuariosComponent,
    ProyectoRolesComponent,
    ProyectoOverviewComponent,
    ProyectoTiposItemComponent,
    ProyectoComiteComponent,
    ProyectoAtributosComponent,
    ProyectoFasesTiposItemComponent,
    ProyectoFasesUsuariosComponent,
    ProyectoNotificacionesComponent,
    ProyectoSolicitudesComponent,
    ProyectoRelacionesComponent,
    ProyectoItemsRevisarComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    CommonModule,
    ToastrModule.forRoot(),
    NgxPaginationModule,
    NgSelectModule,
    FormsModule,
    NgxPermissionsModule.forChild({
      permissionsIsolate: true, 
      rolesIsolate: true}),
    NgxChartsModule,
    NgxGraphModule,
    MisProyectosDetailRoutingModule
  ],
  providers: [
    DatePipe
  ]
})
export class MisProyectosDetailModule { }
