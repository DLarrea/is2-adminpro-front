import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Fase } from 'src/app/_models/fase';
import { FaseService } from 'src/app/_services/fase.service';
import { TipoItemFaseService } from 'src/app/_services/tipoItemFase';
import { TipoItemService } from 'src/app/_services/tipoItem.service';
import { RolProyectoService } from 'src/app/_services/rolProyecto.service';
import { ProyectoService } from 'src/app/_services/proyecto.service';

declare var $:any;
@Component({
  selector: 'app-proyecto-fases-usuarios',
  templateUrl: './proyecto-fases-usuarios.component.html',
  styleUrls: ['./proyecto-fases-usuarios.component.css']
})
export class ProyectoFasesUsuariosComponent implements OnInit {

  proyectoId = null;
  faseId = null;
  p: number = 1
  constructor(private route: ActivatedRoute, private toastr: ToastrService, 
    private apiFase: FaseService, 
    private apiRolProyecto: RolProyectoService,
    private apiProyecto: ProyectoService) {
    this.route.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });

    this.route.paramMap.subscribe(params => {
      this.faseId = params.get("idFase");
    });
  }

  fase: Fase = new Fase();
  roles = [];
  rol = {
    nombre: null
  }
  ngOnInit(): void {
    this.getFase();
    this.getRolesFase();
    this.getUsers();
  }

  getFase() {
    this.apiFase.getById(this.faseId).subscribe(
      data => {
        this.fase = data;
      }
    )
  }


  getRolesFase(){
    this.apiRolProyecto.getByFase(this.proyectoId, this.faseId).subscribe(
      data => {
        this.roles = data;
        console.log(this.roles);
      }
    )
  }

  postRol(){
    this.rol["fase"] = this.faseId;
    this.rol["proyecto"] = this.proyectoId;
    this.apiRolProyecto.post(this.rol).subscribe(
      data => {
        this.rol.nombre = null;
        this.toastr.success("Rol agregado a la fase");
        this.getRolesFase();
      }
    )
  }

  rolPermisos = {
    rol: {
      id: null,
      nombre: "",
      proyecto: null
    },
    permisos_asignados: [],
    permisos_disponibles: []
  }
  rolPermisosModal = null;
  rolPermisosLoading = false;
  obtenerPermisos(rol) {
    this.rolPermisosLoading = true;
    this.rolPermisosModal = rol;
    this.rolPermisos.permisos_asignados = [];
    this.rolPermisos.permisos_disponibles = [];
    this.rolPermisos.rol.nombre = null;
    this.apiRolProyecto.getPermisos(rol.id).subscribe(
      data => {
        this.rolPermisos = data;
        this.rolPermisos.permisos_disponibles = this.rolPermisos.permisos_disponibles.filter((it)=>{
          return it.tipo_permiso == "F";
        }); 
        this.rolPermisosLoading = false;
      }
    )
  }

  asignarPermiso(idPermiso) {
    this.apiRolProyecto.asignarPermiso(this.rolPermisosModal.id, idPermiso).subscribe(
      data => { this.obtenerPermisos(this.rolPermisosModal) },
      error => { }
    )
  }

  desasignarPermiso(idPermiso) {
    this.apiRolProyecto.desasignarPermiso(this.rolPermisosModal.id, idPermiso).subscribe(
      data => { this.obtenerPermisos(this.rolPermisosModal) },
      error => { }
    )
  }

  eliminarRol(id) {
    this.apiRolProyecto.delete(id).subscribe(
      data => {
        this.toastr.success("Rol eliminado");
        this.getRolesFase();
      },
      error => {
        let err = error.error;

        if (err.error) {
          if (err.hasOwnProperty("mensaje")) {
            this.toastr.error(err.mensaje);
          } else {
            this.toastr.error("Error al eliminar rol");
          }
        } else {
          this.toastr.error("Error al eliminar rol");
        }
      }
    )
  }

  users = [];
  getUsers() {
    this.apiProyecto.getUsers(this.proyectoId).subscribe(
    data => {
      this.users = data;
    },
    error => {
      this.toastr.error("Error al recuperar usuarios", "Usuarios");
    } );
  }

  rolActual = null;
  rolActualPut = null;
  hasRolActual = false;
  userActual = null;
  getUserRoles(user){
    this.hasRolActual = false;
    this.rolActual = null;
    this.userActual = user;
    this.apiRolProyecto.getByUserId(user.id, this.proyectoId).subscribe(
      data =>{
        let roles: any[] = data;
        for(let i=0; i<roles.length;i++){
          let r = roles[i];
          if(r.fase != null && r.fase == this.faseId){
            this.rolActual = r.id;
            this.rolActualPut = r.id;
            this.hasRolActual = true;
            break;
          }
        }
        console.log(data);
      }
    )
  }

  guardarRolUser(){
    
    if(!this.hasRolActual){
      let rolFase = {
        id: null,
        user: this.userActual.id,
        rol: this.rolActual
      }
      this.apiRolProyecto.postRolFase(rolFase).subscribe(data=> {
        this.toastr.success("Rol Asignado");
      })
    }else{
      let rolFase = {
        user: this.userActual.id,
        rol: this.rolActual
      }
      this.apiRolProyecto.putRolFase(this.rolActualPut, this.userActual.id, rolFase).subscribe(data=> {
        this.toastr.success("Rol Asignado");
      })
    }

  }
}
