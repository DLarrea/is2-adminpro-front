import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoFasesUsuariosComponent } from './proyecto-fases-usuarios.component';

describe('ProyectoFasesUsuariosComponent', () => {
  let component: ProyectoFasesUsuariosComponent;
  let fixture: ComponentFixture<ProyectoFasesUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoFasesUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoFasesUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
