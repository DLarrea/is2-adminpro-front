import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProyectoService } from 'src/app/_services/proyecto.service';
import { User } from 'src/app/_models/user';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/_services/user.service';
import { RolProyectoService } from 'src/app/_services/rolProyecto.service';
import { ComiteService } from 'src/app/_services/comite.service';
declare var $: any;
@Component({
  selector: 'app-proyecto-comite',
  templateUrl: './proyecto-comite.component.html',
  styleUrls: ['./proyecto-comite.component.css']
})
export class ProyectoComiteComponent implements OnInit {

  proyectoId = null;
  //Variables de pantalla principal
  users: any[] = [];
  loading = true;

  asignados = [];

  //Variable de paginado
  p: number = 1;

  constructor(private route: ActivatedRoute, 
    private toastr: ToastrService, private apiProyecto: ProyectoService,
    private apiComite: ComiteService) {
    this.route.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });
  }


  ngOnInit(): void {
    this.getComite();
    this.getProyecto();
  }

  proyecto = null;
  getProyecto(){
    this.apiProyecto.getById(this.proyectoId).subscribe(
      data => {
        this.proyecto = data;
        console.log(this.proyecto);
      }
    )
  }

  getComite() {
    this.loading = true;
    this.users = [];
    this.asignados = [];
    this.apiComite.get(this.proyectoId).subscribe(
    data => {
      console.log(data);
      this.users = data;
      this.loading = false;
    },
    error => {
      this.loading = false;
      this.toastr.error("Error al recuperar usuarios", "Usuarios");
    } );
  }

  deleteMiembro(miembro){
    miembro["activo"] = false;
    this.apiComite.put(miembro).subscribe(
      data => {
        this.toastr.success("Miembro eliminado del comite");
        this.getComite();
      },
      error => {
        this.toastr.error("Error al eliminar miembro del comite");
      }
    )
  }

  asignables: any [] = [];
  getUsers(){
    this.asignables = [];
    this.apiProyecto.getUsers(this.proyectoId).subscribe(
      data => {
        this.asignables = data;
      },
      error => {

      }
    )
  }

  postMiembro(id){
    let body = {
      miembro: id,
      proyecto: this.proyectoId
    }
    this.apiComite.post(body).subscribe(
      data => {
        this.toastr.success("Miembro agregado");
        $("#listUsers").modal("hide");
        this.getComite();
      },
      error => {
        this.toastr.error("El usuario ya se encuentra en el comite");
      }
    )
  }
}
