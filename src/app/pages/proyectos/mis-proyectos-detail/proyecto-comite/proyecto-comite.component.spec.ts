import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoComiteComponent } from './proyecto-comite.component';

describe('ProyectoComiteComponent', () => {
  let component: ProyectoComiteComponent;
  let fixture: ComponentFixture<ProyectoComiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoComiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoComiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
