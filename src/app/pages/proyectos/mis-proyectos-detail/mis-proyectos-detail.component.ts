import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProyectoService } from 'src/app/_services/proyecto.service';
import { Proyecto } from 'src/app/_models/proyecto';
import { PermService } from 'src/app/_services/perm.service';
import { NgxPermissionsService } from 'ngx-permissions';
import { EstadoProyectoService } from 'src/app/_services/estadoProyecto.service';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';

declare var $:any;
@Component({
  selector: 'app-mis-proyectos-detail',
  templateUrl: './mis-proyectos-detail.component.html',
  styleUrls: ['./mis-proyectos-detail.component.css']
})
export class MisProyectosDetailComponent implements OnInit {

  proyectoId = null;
  proyecto: Proyecto = new Proyecto();
  ready = false;
  constructor(
    private route: ActivatedRoute, 
    private apiProyecto: ProyectoService,
    private permService: PermService,
    private ngxP: NgxPermissionsService,
    private apiEstadoProyecto: EstadoProyectoService,
    private toastr: ToastrService,
    private datePipe: DatePipe) { 
    this.proyectoId = route.snapshot.paramMap.get("id");
  }

  perms = []
  async ngOnInit() {
    this.getEstadosProyectos();
    this.rolesUsuario();
    let u = JSON.parse(sessionStorage.getItem("currentUser"))
  
    await this.permService.getPermisosProyecto(u.id, this.proyectoId)
    .toPromise()
    .then(
      res => {
        let perms = [];
        for( let i=0; i< res.length; i++ ){
          perms.push(res[i].permiso);
        }
        this.ngxP.loadPermissions(perms);
        sessionStorage.setItem('pp',JSON.stringify(res))
      }
    )

    this.get();
  }

  cancelado = false;
  desarrollo = false;
  pendiente = false;
  get(){
    this.ready = false;
    this.apiProyecto.getById(this.proyectoId).subscribe(
      data => {
        this.proyecto = data;
        this.cancelado = this.proyecto.estadoProyecto.nombre == "CANCELADO";
        this.desarrollo = this.proyecto.estadoProyecto.nombre == "EN DESARROLLO";
        this.pendiente = this.proyecto.estadoProyecto.nombre == "PENDIENTE";
        this.ready = true;
      }
    )
  }
  estados: any[] = [];
  
  eDesarrollo;
  eCancelado;
  getEstadosProyectos(){
    this.apiEstadoProyecto.get().subscribe(
      data => {
        this.estados = data;
        this.eDesarrollo = this.estados.find(it => {
          return it.nombre == "EN DESARROLLO";
        });
        this.eCancelado = this.estados.find(it => {
          return it.nombre == "CANCELADO";
        });
        console.log(this.estados);
        console.log(this.eDesarrollo,this.eCancelado);
      }
    )
  }

  iniciarProyecto(){
    this.proyecto.estadoProyecto = this.eDesarrollo.id;
    this.proyecto.fecha_inicio = this.datePipe.transform(new Date(), 'dd/MM/yyyy');
    this.apiProyecto.iniciarProyecto(this.proyecto).subscribe(
      data => {
        this.toastr.success("El proyecto fue iniciado");
        $("#iniciarProyecto").modal("hide");
        this.get();
      },
      error => {
        this.toastr.error("El proyecto no posee fases");
      }
    )
  }

  cancelarProyecto(){
    console.log(this.proyecto.observacion);
    if(this.proyecto.observacion != null && this.proyecto.observacion != ''){
      this.proyecto.estadoProyecto = this.eCancelado.id;
      this.proyecto.fecha_inicio = this.datePipe.transform(new Date(), 'dd/MM/yyyy');
      this.apiProyecto.put(this.proyecto).subscribe(
        data => {
          this.toastr.success("El proyecto fue cancelado");
          $("#cancelarProyecto").modal("hide");
          this.get();
        }
      )
    }else{
      this.toastr.error('Complete motivo de la cancelación');
    }
    
  }

  rolesAsignadosUsuario = [];
  isGerente = false;
  rolesUsuario(){
    let uId = JSON.parse(sessionStorage.getItem("currentUser")).id;
    this.apiProyecto.rolesUsuarioProyecto(uId, this.proyectoId).subscribe(
      data => {
        this.rolesAsignadosUsuario = data;
        this.rolesAsignadosUsuario.forEach(el => {
          if(el.rol.nombre == 'Gerente'){
            this.isGerente = true;
          }
        });
      }
    )
  }
}
