import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoTiposItemComponent } from './proyecto-tipos-item.component';

describe('ProyectoTiposItemComponent', () => {
  let component: ProyectoTiposItemComponent;
  let fixture: ComponentFixture<ProyectoTiposItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoTiposItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoTiposItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
