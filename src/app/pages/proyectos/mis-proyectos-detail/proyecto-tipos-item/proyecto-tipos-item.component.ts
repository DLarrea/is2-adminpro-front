import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/_models/user';
import { ToastrService } from 'ngx-toastr';
import { TipoItemService } from 'src/app/_services/tipoItem.service';
import { TipoItem } from 'src/app/_models/tipoItem';

@Component({
  selector: 'app-proyecto-tipos-item',
  templateUrl: './proyecto-tipos-item.component.html',
  styleUrls: ['./proyecto-tipos-item.component.css']
})
export class ProyectoTiposItemComponent implements OnInit {

  proyectoId = null;
  constructor(private route: ActivatedRoute,
    private toastr: ToastrService,
    private apiTipoItem: TipoItemService) {
    this.route.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });
  }

  loading = true;
  tipoItems:TipoItem[] = [];

  tipoItem = {
    nombre: null,
    descripcion: null
  }
  //Variables de modal agregar rol
  user = new User();
  loadingSave = false;

  //Variable de paginado
  p: number = 1;

  rol: string = "";
  agregar = false;

  async ngOnInit() {
    this.getTipoItems();
  }

  getTipoItems() {
    this.loading = true;
    this.apiTipoItem.getByProyecto(this.proyectoId).subscribe(data => {
      this.tipoItems = data;
      this.loading = false;
      console.log(this.tipoItems);
    },
    error => {
      this.toastr.error("Error al recuperar tipos de items", "Fases");
    });
  }

  postTipoItem(){
    if(this.tipoItem.nombre !=null && this.tipoItem.descripcion != null){
      this.loadingSave = true;
      const body = {
        nombre: this.tipoItem.nombre,
        descripcion: this.tipoItem.descripcion,
        fase: null,
        proyecto: this.proyectoId
      }
      this.apiTipoItem.post(body).subscribe(
        data => {
          this.toastr.success("Tipo de item creado");
          this.loadingSave = false;
          this.getTipoItems();
        },
        error => {
          this.toastr.error("Error al crear tipo de item");
          this.loadingSave = false;
        }
      )
    }else{
      this.toastr.error("Complete los campos");
    }
  }

  deleteTipoItem(id){
    this.apiTipoItem.delete(id).subscribe(
      data => {
        this.toastr.success("Tipo de item eliminado");
        this.getTipoItems();
      }, 
      error => {
        this.toastr.error("Error al eliminar tipo de item");
      }
    )
  }

  putTipoItem(target: TipoItem){
    if(target.nombre != null && target.nombre != "" && target.descripcion != null && target.descripcion != ""){
      this.apiTipoItem.put(target).subscribe(
        data => {
          this.toastr.success("Tipo de item editado");
          this.getTipoItems();
        }, 
        error => {
          this.toastr.error("Error al editar tipo de item");
        }
      )
    }else{
      this.toastr.error("El nombre no puede quedar vacío")
    }
  }
  aa = [];
  ad = [];
  loadingGestionar = false;
  gestionId = null;
  gestion: TipoItem = new TipoItem();
  gestionar(id){
    this.loadingGestionar = true;
    this.apiTipoItem.gestionar(id).subscribe(
      data => {
        this.aa = data.asignados;
        this.ad = data.disponibles;

        for( let i=0; i< this.aa.length; i++ ){
          if(this.aa[i].type == "A"){
            this.aa[i].type = "ARCHIVO"
          }else if(this.aa[i].type == "T"){
            this.aa[i].type = "TEXTO"
          }else {
            this.aa[i].type = "NUMÉRICO"
          }
        }

        for( let i=0; i< this.ad.length; i++ ){
          if(this.ad[i].type == "A"){
            this.ad[i].type = "ARCHIVO"
          }else if(this.ad[i].type == "T"){
            this.ad[i].type = "TEXTO"
          }else {
            this.ad[i].type = "NUMÉRICO"
          }
        }

        this.loadingGestionar = false;
      }
    )
  }

  asignar(idTipoItem, idAtributo){
    this.apiTipoItem.gestionarAsignar(idTipoItem, idAtributo).subscribe(
      data => {
        this.toastr.success("Atributo asignado");
        this.gestionar(this.gestionId);
      },
      error => {
        this.toastr.success("Error al asignar atributo");
      }
    )
  }

  desasignar(idTipoItem, idAtributo){
    this.apiTipoItem.gestionarDesasignar(idTipoItem, idAtributo).subscribe(
      data => {
        this.toastr.success("Atributo desasignado");
        this.gestionar(this.gestionId);
      },
      error => {
        this.toastr.success("Error al desasignar atributo");
      }
    )
  }

  importables = [];
  getImportables(){
    this.apiTipoItem.getImportables(this.proyectoId).subscribe(
      data => {
        this.importables = data;
        console.log(this.importables)
      }
    )
  }

  postByImport(obj){
    obj.tipoItem["proyecto"] = this.proyectoId;
    this.apiTipoItem.postByImport(obj).subscribe(
      data => {
        this.toastr.success("Tipo de item importado");
        this.getTipoItems();
      },
      error => {
        this.toastr.error("Error al importar tipo de item");
      }
    )
  }

}
