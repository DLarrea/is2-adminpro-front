import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from "@angular/common";
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ToastrModule } from "ngx-toastr";
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule } from "@angular/forms";
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPermissionsModule } from 'ngx-permissions';

import { ProyectoFasesItemsRoutingModule } from './proyecto-fases-items-routing.module';
import { ProyectoFasesItemsComponent } from './proyecto-fases-items.component';
import { environment } from 'src/environments/environment';
import { ProyectoFasesItemsCrearComponent } from './proyecto-fases-items-crear/proyecto-fases-items-crear.component';
import { ProyectoFasesItemsListarComponent } from './proyecto-fases-items-listar/proyecto-fases-items-listar.component';
import { ProyectoFasesItemsGestionarComponent } from './proyecto-fases-items-gestionar/proyecto-fases-items-gestionar.component';
import { ProyectoFasesItemsHistorialComponent } from './proyecto-fases-items-historial/proyecto-fases-items-historial.component';
import { ProyectoFasesLineasBaseComponent } from './proyecto-fases-lineas-base/proyecto-fases-lineas-base.component';


@NgModule({
  declarations: [ProyectoFasesItemsComponent, ProyectoFasesItemsCrearComponent, ProyectoFasesItemsListarComponent, ProyectoFasesItemsGestionarComponent, ProyectoFasesItemsHistorialComponent, ProyectoFasesLineasBaseComponent],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    CommonModule,
    ToastrModule.forRoot(),
    NgxPaginationModule,
    NgSelectModule,
    FormsModule,
    NgxPermissionsModule.forChild({
      permissionsIsolate: true, 
      rolesIsolate: true}),
    ProyectoFasesItemsRoutingModule
  ]
})
export class ProyectoFasesItemsModule { }
