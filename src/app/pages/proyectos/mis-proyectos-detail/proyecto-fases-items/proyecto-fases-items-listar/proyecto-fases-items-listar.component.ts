import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Fase } from 'src/app/_models/fase';
import { FaseService } from 'src/app/_services/fase.service';
import { ItemService } from 'src/app/_services/item.service';
import Hashids from 'hashids'

@Component({
  selector: 'app-proyecto-fases-items-listar',
  templateUrl: './proyecto-fases-items-listar.component.html',
  styleUrls: ['./proyecto-fases-items-listar.component.css']
})
export class ProyectoFasesItemsListarComponent implements OnInit {

  proyectoId = null;
  faseId = null;
  fase: Fase = new Fase();
  items = [];
  p: number = 1;
  hashids = new Hashids('adminpro', 5)

  constructor(
    private route: ActivatedRoute, 
    private toastr: ToastrService, 
    private apiFase: FaseService,
    private apiItem: ItemService) {
    this.route.parent.parent.parent.params.subscribe(
      (params) => {
        console.log(params);
        this.proyectoId = params.id;
      });

    this.route.parent.parent.params.subscribe(
      (params) => {
        console.log(params);
        this.faseId = params.idFase;
        this.getFase();
        this.getItems();
      });
  }

  ei = {
    "DE": "DESARROLLO",
    "PP": "PENDIENTE DE APROBACIÓN",
    "AP": "APROBADO",
    "CA": "CANCELADO",
    "RE": "EN REVISIÓN"
  }
  
  eiColor = {
    "DE": "badge badge-primary",
    "PP": "badge badge-warning",
    "AP": "badge badge-success",
    "CA": "badge badge-secondary",
    "RE": "badge badge-danger"
  }
  
  ngOnInit(): void {
  }

  getFase(){
    this.apiFase.getById(this.faseId).subscribe(
      data => {
        this.fase = data;
      }
    )
  }

  getItems(){
    this.apiItem.get(this.faseId).subscribe(
      data => {
        this.items = data;
        this.items.forEach(it => {
          it["codigo"] = this.hashids.encode(it.id);
        });
      }
    )
  }

  getImpacto(id){
    this.apiItem.getImpacto(id).subscribe(
      data => {
        this.impactoDatos = data;
        console.log(this.impactoDatos);
        for(let i=0;i<this.impactoDatos.items_afectados.length;i++){
          let it = this.impactoDatos.items_afectados[i];
          it["codigo"] = this.hashids.encode(it.id);
        }
      }
    )
  }

  impactoTarget = null;
  impactoDatos = null;
  openModalImpacto(target){
    this.impactoTarget = null;
    this.impactoDatos = null;
    this.impactoTarget = target;
    this.getImpacto(this.impactoTarget.id);
  }
}
