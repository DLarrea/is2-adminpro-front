import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoFasesItemsListarComponent } from './proyecto-fases-items-listar.component';

describe('ProyectoFasesItemsListarComponent', () => {
  let component: ProyectoFasesItemsListarComponent;
  let fixture: ComponentFixture<ProyectoFasesItemsListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoFasesItemsListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoFasesItemsListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
