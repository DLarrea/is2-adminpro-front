import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoFasesItemsHistorialComponent } from './proyecto-fases-items-historial.component';

describe('ProyectoFasesItemsHistorialComponent', () => {
  let component: ProyectoFasesItemsHistorialComponent;
  let fixture: ComponentFixture<ProyectoFasesItemsHistorialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoFasesItemsHistorialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoFasesItemsHistorialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
