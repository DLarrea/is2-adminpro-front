import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Item } from 'src/app/_models/item';
import { ItemService } from 'src/app/_services/item.service';

@Component({
  selector: 'app-proyecto-fases-items-historial',
  templateUrl: './proyecto-fases-items-historial.component.html',
  styleUrls: ['./proyecto-fases-items-historial.component.css']
})
export class ProyectoFasesItemsHistorialComponent implements OnInit {

  proyectoId = null;
  faseId = null;
  itemId = null;
  items: Item[] = [];
  modalDetalles= {
    nombre: "",
    descripcion: "",
    prioridad: null,
    tipo: "",
    values: [],
    version: null
  };
  constructor(
    private route:ActivatedRoute,
    private apiItem: ItemService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.itemId = route.snapshot.paramMap.get("idItem");
    this.route.parent.parent.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });

    this.route.parent.parent.params.subscribe(
      (params) => {
        this.faseId = params.idFase;
      });
  }

  ngOnInit(): void {
    this.getHistorial();
  }

  restaurar = false;
  getHistorial(){
    this.apiItem.getHistorial(this.itemId).subscribe(
      data => {
        this.items = data;
        for(let i=0; i<this.items.length;i++){
          if(this.items[i].current){
            if(this.items[i].estado.nombre == 'DE'){
              this.restaurar = true;
            }
          }
        }
        console.log(this.items);
      }
    )
  }

  mapModalDetalles(item: Item){
    this.modalDetalles.nombre = item.nombre;
    this.modalDetalles.prioridad = item.prioridad;
    this.modalDetalles.tipo = item.tipo.nombre;
    this.modalDetalles.version = item.version;
    this.modalDetalles.descripcion = item.descripcion;
    this.modalDetalles.values = JSON.parse(item.values);
  }

  postHistorial(itemToRestore:Item){
    
    let itRestore:Item = Object.assign({}, itemToRestore);
    let oldCurrent:Item = null;

    for(let i=0; i<this.items.length;i++){
      if(this.items[i].current){
        oldCurrent = this.items[i];
        break;
      }
    }

    itRestore.tipo = itRestore.tipo.id;
    itRestore.estado = itRestore.estado.id;
    itRestore.ant = oldCurrent.id;
    itRestore.sig = null;
    itRestore.id = null;
    itRestore.current = true;
    itRestore.version = oldCurrent.version + 1; 

    const body = {
      prev_id: oldCurrent.id,
      item: itRestore
    }

    this.apiItem.post(body).subscribe(
      data => {
        this.toastr.success("Item restaurado");
        this.router.navigate(['mis-proyectos', this.proyectoId, 'fases', this.faseId, 'items']);
      }
    )
  }

}
