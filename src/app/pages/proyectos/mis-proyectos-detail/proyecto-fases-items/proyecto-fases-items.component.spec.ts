import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoFasesItemsComponent } from './proyecto-fases-items.component';

describe('ProyectoFasesItemsComponent', () => {
  let component: ProyectoFasesItemsComponent;
  let fixture: ComponentFixture<ProyectoFasesItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoFasesItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoFasesItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
