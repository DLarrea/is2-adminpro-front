import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Item } from 'src/app/_models/item';
import { EstadoItemService } from 'src/app/_services/estadoItem.service';
import { ItemService } from 'src/app/_services/item.service';
import { TipoItemService } from 'src/app/_services/tipoItem.service';
declare var $:any;
@Component({
  selector: 'app-proyecto-fases-items-gestionar',
  templateUrl: './proyecto-fases-items-gestionar.component.html',
  styleUrls: ['./proyecto-fases-items-gestionar.component.css']
})
export class ProyectoFasesItemsGestionarComponent implements OnInit {

  proyectoId = null;
  faseId = null;
  itemId = null;
  estados = [];
  atributos = [];
  values = [];
  currentItem: Item = new Item();
  currentEstado = null;
  item: Item = new Item();
  estadosItem = [];
  constructor(
    private route:ActivatedRoute,
    private apiItem: ItemService,
    private toastr: ToastrService,
    private apiTipoItem: TipoItemService,
    private apiEstadoItem: EstadoItemService,
    private router: Router) {
    this.itemId = route.snapshot.paramMap.get("idItem");
    this.route.parent.parent.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });

    this.route.parent.parent.params.subscribe(
      (params) => {
        this.faseId = params.idFase;
      });
  }

  ngOnInit(): void {
    this.getItem();
    this.getEstadosItem();
  }

  getItem(){
    this.apiItem.getById(this.itemId).subscribe(
      data => {
        this.currentItem = Object.assign({}, data);
        this.item = data;
        this.getAtributos(data.tipo.tipoitem);
        let attr = JSON.parse(this.item.values);
        for(let i=0;i<attr.length;i++){
          this.values.push(attr[i].value);
        }
      }
    )
  }

  eDE = null;
  ePP = null;
  eAP = null;
  eCA = null;
  ei = {
    "DE": "DESARROLLO",
    "PP": "PENDIENTE DE APROBACIÓN",
    "AP": "APROBADO",
    "CA": "CANCELADO"
  }
  eia = {
    "DE": "alert-primary",
    "PP": "alert-warning",
    "AP": "alert-success",
    "CA": "alert-danger"
  }
  getEstadosItem() {
    this.apiEstadoItem.getEstados().subscribe(data => {
      this.estados = data;

      this.eDE = this.estados.find(it => {
        return it.nombre == 'DE';
      });

      this.ePP = this.estados.find(it => {
        return it.nombre == 'PP';
      });

      this.eAP = this.estados.find(it => {
        return it.nombre == 'AP';
      });

      this.eCA = this.estados.find(it => {
        return it.nombre == 'CA';
      });
    },
    error => {
      this.toastr.error("Error al recuperar tipos de items", "Agregar Item");
    });
  }

  getAtributos(idTipoItem){
    this.atributos = [];
    this.values = [];
    this.apiTipoItem.getTipoItemAtributos(idTipoItem).subscribe(
      data => {
        this.atributos = data;
        // for(let i=0; i<this.atributos.length; i++){
        //   let obj: any;
        //   this.values.push(obj);
        // }
        // if(this.atributos.length == 0){
        //   this.toastr.warning("El tipo de item no tiene atributos");
        // }
        if(this.item.estado.nombre != "DE"){
          setTimeout(()=> {
            $('select').attr('disabled',true);
            $('textarea').prop('disabled',true);
            $('input[type=text]').attr('disabled',true);
            $('input[type=number]').attr('disabled',true);
            $('a').attr('disabled',true);
          }, 1000)
        }
      },
      error => {
        this.toastr.error("Error al recuperar atributos");
      }
    );
  }

  async handleFiles(files:FileList, j){
    let file: File = files.item(0);
    let filename = files.item(0).name;
    let base64 = await this.toBase64(file);
    this.values[j] = { file: base64, filename: filename}
  }

  toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  guardarItem(aprobacion){
    
    if(this.item.estado.nombre == "DE"){

      if(aprobacion) {
        this.item.estado = this.ePP;
      }
      let valuesJson = [];
      for( let i=0; i< this.values.length; i++){
        valuesJson.push({"value": this.values[i], "type": this.atributos[i].type})
      }
  
      this.item.current = true;
      this.item.version += 1;
      this.item.values = JSON.stringify(valuesJson);
      this.item.ant = this.currentItem.id;
      this.item.id = null;
      this.item.estado = this.item.estado.id;
      this.item.tipo = this.item.tipo.id;

      const body = {
        prev_id: this.currentItem.id,
        item: this.item
      }
  
      this.apiItem.post(body).subscribe(
        data => {
          this.toastr.success("Item actualizado correctamente");
          this.router.navigate(['mis-proyectos', this.proyectoId, 'fases', this.faseId, 'items']);
        }
      )
  
    }else {
      this.toastr.error("Solo se pueden editar items con estado EN DESARROLLO")
    }
    
  }

  cancelarItem(){
    this.currentItem.estado = this.eCA.id;
    this.currentItem.tipo = this.currentItem.tipo.id;
    this.apiItem.put(this.currentItem).subscribe(
      data => {
        this.toastr.success("Item cancelado");
        this.router.navigate(['mis-proyectos', this.proyectoId, 'fases', this.faseId, 'items']);
      },
      error => {
        this.toastr.error(error.error.mensaje);
      }
    )
  }

  solicitarAprobacionItem(){
    this.currentItem.estado = this.ePP.id;
    this.currentItem.tipo = this.currentItem.tipo.id;
    this.apiItem.put(this.currentItem).subscribe(
      data => {
        this.toastr.success("Item en revisión");
        this.router.navigate(['mis-proyectos', this.proyectoId, 'fases', this.faseId, 'items']);
      }
    )
  }

  aprobarItem(){
    this.currentItem.estado = this.eAP.id;
    this.currentItem.tipo = this.currentItem.tipo.id;
    this.apiItem.put(this.currentItem).subscribe(
      data => {
        this.toastr.success("Item aprobado");
        this.router.navigate(['mis-proyectos', this.proyectoId, 'fases', this.faseId, 'items']);
      },
      error => {
        this.toastr.error("El item no posee padre ni antecesor");
      }
    )
  }

  desaprobarItem(){
    this.currentItem.estado = this.eDE.id;
    this.currentItem.tipo = this.currentItem.tipo.id;
    this.apiItem.put(this.currentItem).subscribe(
      data => {
        this.toastr.success("Item desaprobado");
        this.router.navigate(['mis-proyectos', this.proyectoId, 'fases', this.faseId, 'items']);
      }
    )
  }

  desarrollarItem(){
    this.currentItem.estado = this.eDE.id;
    this.currentItem.tipo = this.currentItem.tipo.id;
    this.apiItem.put(this.currentItem).subscribe(
      data => {
        this.toastr.success("El item se ha movido a desarrollo");
        this.router.navigate(['mis-proyectos', this.proyectoId, 'fases', this.faseId, 'items']);
      }
    )
  }
}
