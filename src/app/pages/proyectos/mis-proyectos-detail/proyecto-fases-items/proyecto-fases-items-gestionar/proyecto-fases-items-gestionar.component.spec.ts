import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoFasesItemsGestionarComponent } from './proyecto-fases-items-gestionar.component';

describe('ProyectoFasesItemsGestionarComponent', () => {
  let component: ProyectoFasesItemsGestionarComponent;
  let fixture: ComponentFixture<ProyectoFasesItemsGestionarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoFasesItemsGestionarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoFasesItemsGestionarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
