import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { PermService } from 'src/app/_services/perm.service';

@Component({
  selector: 'app-proyecto-fases-items',
  templateUrl: './proyecto-fases-items.component.html',
  styleUrls: ['./proyecto-fases-items.component.css']
})
export class ProyectoFasesItemsComponent implements OnInit {

  proyectoId = null;
  faseId = null;
  constructor(
    private permService: PermService,
    private route:ActivatedRoute,
    private ngxP: NgxPermissionsService
    ) {
    this.route.parent.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
        this.tryPerm();
      });

    this.route.parent.params.subscribe(
      (params) => {
        this.faseId = params.idFase;
        this.tryPerm();
      });
  }

  async ngOnInit() {
  }

  async tryPerm(){
    if(this.proyectoId != null && this.faseId != null){
      let u = JSON.parse(sessionStorage.getItem("currentUser"));
      console.log(u);
      await this.permService.getPermisosProyectoFase(u.id, this.proyectoId, this.faseId)
      .toPromise()
      .then(
        res => {
          let perms = [];
          for( let i=0; i< res.length; i++ ){
            perms.push(res[i].permiso);
          }
          this.ngxP.loadPermissions(perms);
          sessionStorage.removeItem('pf');
          sessionStorage.setItem('pf',JSON.stringify(res))
        }
      )
    }
  }
}
