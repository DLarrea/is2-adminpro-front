import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoFasesItemsCrearComponent } from './proyecto-fases-items-crear.component';

describe('ProyectoFasesItemsCrearComponent', () => {
  let component: ProyectoFasesItemsCrearComponent;
  let fixture: ComponentFixture<ProyectoFasesItemsCrearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoFasesItemsCrearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoFasesItemsCrearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
