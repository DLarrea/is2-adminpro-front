import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Item } from 'src/app/_models/item';
import { EstadoItemService } from 'src/app/_services/estadoItem.service';
import { FirebaseService } from 'src/app/_services/firebase.service';
import { ItemService } from 'src/app/_services/item.service';
import { TipoItemService } from 'src/app/_services/tipoItem.service';
declare var $:any;
@Component({
  selector: 'app-proyecto-fases-items-crear',
  templateUrl: './proyecto-fases-items-crear.component.html',
  styleUrls: ['./proyecto-fases-items-crear.component.css']
})
export class ProyectoFasesItemsCrearComponent implements OnInit {

  proyectoId = null;
  faseId = null;
  p: number = 1;
  tipoItems = [];
  atributos = [];
  values = [];
  item: Item = new Item();
  estadosItem = [];
  ei = {
    "DE": "DESARROLLO",
    "PP": "PENDIENTE DE APROBACIÓN",
    "AP": "APROBADO",
    "CA": "CANCELADO"
  }
  constructor(
    private route: ActivatedRoute, 
    private toastr: ToastrService,
    private apiTipoItem: TipoItemService,
    private apiEstadoItem: EstadoItemService,
    private apiItem: ItemService,
    private router: Router,
    private apiFirebase: FirebaseService
  ) {
    this.route.parent.parent.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });

    this.route.parent.parent.params.subscribe(
      (params) => {
        this.faseId = params.idFase;
      });
  }

  ngOnInit(): void {
    this.getTipoItems();
    this.getEstadosItem();
  }

  getEstadosItem(){
    this.apiEstadoItem.getEstados().subscribe(
      data => {
        this.estadosItem = data;
        this.item.estado = this.estadosItem.find( it => {
          return it.nombre == "DE";
        }).id;
      }
    )
  }
  tipoItemsFase: any[] = [];
  getTipoItems() {
    this.apiTipoItem.get(this.faseId).subscribe(data => {
      this.tipoItemsFase = data;
      for(let i=0; i<data.length;i++){
        this.tipoItems.push(data[i].tipoitem);
      }
      this.getAtributos(this.tipoItems[0].id);
      this.item.tipo = this.tipoItems[0].id;
    },
    error => {
      this.toastr.error("Error al recuperar tipos de items", "Agregar Item");
    });
  }

  getAtributos(idTipoItem){
    this.atributos = [];
    this.values = [];
    this.apiTipoItem.getTipoItemAtributos(idTipoItem).subscribe(
      data => {
        this.atributos = data;
        for(let i=0; i<this.atributos.length; i++){
          let obj: any;
          this.values.push(obj);
        }
        if(this.atributos.length == 0){
          this.toastr.warning("El tipo de item no tiene atributos");
        }
      },
      error => {
        this.toastr.error("Error al recuperar atributos");
      }
    );
  }

  guardarItem(){
    let valuesJson = [];
    for( let i=0; i< this.values.length; i++){
      valuesJson.push({"value": this.values[i], "type": this.atributos[i].type})
    }

    let idTipoItem = Number(this.item.tipo);
    console.log(this.tipoItemsFase);
    for(let i=0; i<this.tipoItemsFase.length;i++){
      if(idTipoItem == this.tipoItemsFase[i]["tipoitem"].id){
        this.item.tipo = this.tipoItemsFase[i]["id"];
      }
    }
    this.item.current = true;
    this.item.version = 1;
    this.item.values = JSON.stringify(valuesJson);

    console.log(this.item)
    const body = {
      item: this.item
    }

    this.apiItem.post(body).subscribe(
      data => {
        let hasFiles = false;
        let firebaseItems = [];
        
        for(let i=0; i < this.values.length; i++){
          if(this.values[i].hasOwnProperty("file")){
            hasFiles = true;
            firebaseItems.push(this.values[i]);
          }
        }
        if(hasFiles){
          this.apiFirebase.post(
            {
              item_id: data.id,
              files: firebaseItems
            }
          ).then(res=>{})
        }
        this.toastr.success("Item creado correctamente");
        this.router.navigate(['mis-proyectos', this.proyectoId, 'fases', this.faseId, 'items']);
      }
    )
  }

  async handleFiles(files:FileList, j){
    let file: File = files.item(0);
    let filename = files.item(0).name;
    let base64 = await this.toBase64(file);
    this.values[j] = { file: base64, filename: filename}
  }

  toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

}
