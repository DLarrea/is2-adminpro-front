import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProyectoFasesItemsCrearComponent } from './proyecto-fases-items-crear/proyecto-fases-items-crear.component';
import { ProyectoFasesItemsGestionarComponent } from './proyecto-fases-items-gestionar/proyecto-fases-items-gestionar.component';
import { ProyectoFasesItemsHistorialComponent } from './proyecto-fases-items-historial/proyecto-fases-items-historial.component';
import { ProyectoFasesItemsListarComponent } from './proyecto-fases-items-listar/proyecto-fases-items-listar.component';
import { ProyectoFasesItemsComponent } from './proyecto-fases-items.component';
import { ProyectoFasesLineasBaseComponent } from './proyecto-fases-lineas-base/proyecto-fases-lineas-base.component';


const routes: Routes = [
  {
    path: "",
    component: ProyectoFasesItemsComponent,
    children: [
      {
        path: "",
        redirectTo: "overview",
        pathMatch: "full"
      },
      {
        path: "overview",
        component: ProyectoFasesItemsListarComponent
      },
      {
        path: "agregar",
        component: ProyectoFasesItemsCrearComponent
      },
      {
        path: "gestionar/:idItem",
        component: ProyectoFasesItemsGestionarComponent
      },
      {
        path: "historial/:idItem",
        component: ProyectoFasesItemsHistorialComponent
      },
      {
        path: "lineas-base",
        component: ProyectoFasesLineasBaseComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProyectoFasesItemsRoutingModule { }
