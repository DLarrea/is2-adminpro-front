import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoFasesLineasBaseComponent } from './proyecto-fases-lineas-base.component';

describe('ProyectoFasesLineasBaseComponent', () => {
  let component: ProyectoFasesLineasBaseComponent;
  let fixture: ComponentFixture<ProyectoFasesLineasBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoFasesLineasBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoFasesLineasBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
