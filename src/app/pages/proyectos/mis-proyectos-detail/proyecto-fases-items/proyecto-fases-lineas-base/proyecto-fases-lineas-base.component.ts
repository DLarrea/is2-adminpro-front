import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Item } from 'src/app/_models/item';
import { LineaBase } from 'src/app/_models/linea-base';
import { EstadoLineaBaseService } from 'src/app/_services/estadoLineaBase';
import { ItemService } from 'src/app/_services/item.service';
import { LineaBaseService } from 'src/app/_services/linea-base.service';
import Hashids from 'hashids';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { SolicitudService } from 'src/app/_services/solicitud.service';
declare var $:any;
@Component({
  selector: 'app-proyecto-fases-lineas-base',
  templateUrl: './proyecto-fases-lineas-base.component.html',
  styleUrls: ['./proyecto-fases-lineas-base.component.css']
})
export class ProyectoFasesLineasBaseComponent implements OnInit {
  
  proyectoId = null;
  faseId = null;

  eLB = {
    "AB":"Abierta",
    "CE": "Cerrada",
    "RO": "Rota",
    "RE": "En revisión"
  }
  user;
  constructor(
    private route: ActivatedRoute, 
    private toastr: ToastrService,
    private apiEstados: EstadoLineaBaseService,
    private apiLB: LineaBaseService,
    private apiItem: ItemService,
    private apiUser: AuthenticationService,
    private apiSolicitud: SolicitudService) {
    this.route.parent.parent.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });

    this.route.parent.parent.params.subscribe(
      (params) => {
        this.faseId = params.idFase;
      });
    
      this.user = this.apiUser.currentUserValue;
  }

  lbAdd: LineaBase = new LineaBase();
  lbEdit: LineaBase = new LineaBase();

  ngOnInit(): void {
    this.getEstados();
    this.getLBs();
  }

  estadosLB: any[] = [];
  estadoAB = null;
  lineasBase: LineaBase[] = [];
  p:number = 1;
  hashids = new Hashids('adminpro', 5)
  getLBs(){
    this.apiLB.get(this.faseId).subscribe(
      data => {
        this.lineasBase = data;
      }
    )
  }

  getEstados(){
    this.apiEstados.getEstados().subscribe(
      data => {
        this.estadosLB = data;
        this.estadoAB = this.estadosLB.find(it => {
          return it.nombre == 'AB';
        });
        console.log(this.estadosLB);
      }
    )
  }

  postLB(){
    this.lbAdd.fase = this.faseId;
    this.lbAdd.estado = this.estadoAB.id;
    if(this.lbAdd.nombre != null && this.lbAdd.descripcion != null){
      this.apiLB.post(this.lbAdd).subscribe(
        data => {
          this.lbAdd = new LineaBase();
          $("#addLineaBase").modal("hide");
          this.toastr.success("Linea base creada");
          this.getLBs();
        }
      )
    }else{
      this.toastr.error("Complete los campos obligatorios");
    }
    
  }

  putLB(){
    if(this.lbEdit.nombre != null && this.lbEdit.descripcion != null){
      let obj:LineaBase = Object.assign({}, this.lbEdit);
      obj.estado = obj.estado.id;
      obj.fase = obj.fase.id;
      this.apiLB.put(obj).subscribe(
        data => {
          this.lbEdit = new LineaBase();
          $("#editLineaBase").modal("hide");
          this.toastr.success("Linea base editada");
          this.getLBs();
        }
      )
    }else{
      this.toastr.error("Complete los campos obligatorios");
    }
    
  }

  items: any[] = [];
  itemsRelacionar: any[] = [];
  itemsSelected = [];
  getItems(){
    this.items = [];
    this.itemsRelacionar = [];
    this.apiItem.get(this.faseId).subscribe(
      data => {
        let datos: any[] = data;
        for(let i=0; i<datos.length;i++){
          datos[i]["codigo"] = this.hashids.encode(datos[i].id);
          if(datos[i].estado.nombre == 'AP' && datos[i].lineaBase == null){
            this.items.push(datos[i]);
            this.itemsSelected.push(false);
          }
        }
      }
    )
  }

  addItem(target, pos){
    let it = Object.assign({}, target);

    if(!this.itemsSelected[pos]){
      this.itemsRelacionar.push(it);
      this.itemsSelected[pos] = true;
    }else{
      for(let i=0;i<this.itemsRelacionar.length;i++){
        if(target.id == this.itemsRelacionar[i].id){
          this.itemsRelacionar.splice(i,1);
          break;
        }
      }
      this.itemsSelected[pos] = false;
    }
  }

  selectedLB = null;
  asociarItems(){

    let datos = [];
    for(let i=0; i<this.itemsRelacionar.length; i++){
      datos.push({id:this.itemsRelacionar[i].id});
    }
    this.apiLB.cerrarLB(this.selectedLB.id, {
      items: datos,
      id: this.selectedLB.id
    }).subscribe(
      data => {
        this.toastr.success("Item asociados y linea base cerrada");
        $("#modalItems").modal("hide");
        this.getLBs();
      }
    )    
  }

  verItems: any[] = [];
  verItemsAsociados(lbId){
    this.verItems = [];
    this.apiItem.getByLB(this.faseId, lbId).subscribe(
      data => {
        this.verItems = data;
        for(let i=0; i<this.verItems.length;i++){
          this.verItems[i]["codigo"] = this.hashids.encode(this.verItems[i].id);
        }
      }
    )
  }

  rupturaTarget: LineaBase = new LineaBase();
  rupturaItems: any[] = [];
  ruptura = {
    nombre: null,
    mensaje: null,
    usuario_creacion: null,
    lineaBase: null,
    proyecto: null,
    item: null  
  }
  openRupturaLB(target:LineaBase){
    this.rupturaTarget = target;
    this.itemsRupturaLB(this.rupturaTarget.id)
    console.log(this.rupturaTarget);
  }

  itemsRupturaLB(lbId){
    this.rupturaItems = [];
    this.apiItem.getByLB(this.faseId, lbId).subscribe(
      data => {
        this.rupturaItems = data;
        for(let i=0; i<this.rupturaItems.length;i++){
          this.rupturaItems[i]["codigo"] = this.hashids.encode(this.rupturaItems[i].id);
        }
        console.log(this.rupturaItems);
      }
    )
  }

  postRuptura(){
    if(this.ruptura.mensaje != null && this.ruptura.item != null){
      this.ruptura.proyecto = Number(this.proyectoId);  
      this.ruptura.lineaBase = this.rupturaTarget.id;
      this.ruptura.usuario_creacion = this.user.id;
      this.ruptura.item = Number(this.ruptura.item);
      this.apiSolicitud.post(this.ruptura).subscribe(
        data => {
          this.toastr.success("Solicitud enviada");
          this.getLBs();
          $("#rupturaLB").modal("hide");
          console.log(this.ruptura);
        }
      )
      
    }else{
      this.toastr.error("Complete los campos obligatorios");
    }
  }
}
