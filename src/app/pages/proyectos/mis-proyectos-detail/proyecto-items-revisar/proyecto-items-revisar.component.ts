import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ItemService } from 'src/app/_services/item.service';
import Hashids from 'hashids'
import { SolicitudService } from 'src/app/_services/solicitud.service';
import { AuthenticationService } from 'src/app/_services/authentication.service';
declare var $:any;
@Component({
  selector: 'app-proyecto-items-revisar',
  templateUrl: './proyecto-items-revisar.component.html',
  styleUrls: ['./proyecto-items-revisar.component.css']
})
export class ProyectoItemsRevisarComponent implements OnInit {

  proyectoId = null;
  user = null;
  constructor(private route: ActivatedRoute, private apiItem: ItemService, 
    private toastr: ToastrService, private apiSolicitud: SolicitudService, private apiUser: AuthenticationService) {
    this.route.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });
      this.user = this.apiUser.currentUserValue;
  }
  p: number = 1;
  items = [];
  hashids = new Hashids('adminpro', 5);
  ei = {
    "DE": "DESARROLLO",
    "PP": "PENDIENTE DE APROBACIÓN",
    "AP": "APROBADO",
    "CA": "CANCELADO",
    "RE": "EN REVISIÓN"
  }
  
  eiColor = {
    "DE": "badge badge-primary",
    "PP": "badge badge-warning",
    "AP": "badge badge-success",
    "CA": "badge badge-secondary",
    "RE": "badge badge-danger"
  }

  ngOnInit(): void {
    this.getItems();
  }
  estadoDE = null;
  getItems(){
    this.apiItem.getItemsRevision(this.proyectoId).subscribe(
      data => {
        this.items = data.items;
        this.estadoDE = data.estadoDE;
        console.log(this.items);
        this.items.forEach(it => {
          it["codigo"] = this.hashids.encode(it.id);
        });
      }
    )
  }

  moverEstadoAnterior(target){
    let item = Object.assign({}, target);
    item.estado = item.estado_ant.id;
    item.estado_ant = null;
    item.tipo = item.tipo.id;
    item.lineaBase = null;

    if(item.ant != null){
      item.ant = item.ant.id;
    }

    this.apiItem.put(item).subscribe(
      data => {
        this.toastr.success("Estado anterior asignado");
        this.getItems();
      }
    )
  }

  moverEstadoDesarrollo(target){
    let item = Object.assign({}, target);
    item.estado = this.estadoDE.id;
    item.estado_ant = null;
    item.tipo = item.tipo.id;
    item.lineaBase = null;

    if(item.ant != null){
      item.ant = item.ant.id;
    }

    this.apiItem.put(item).subscribe(
      data => {
        this.toastr.success("Estado desarrollo asignado");
        this.getItems();
      }
    )
  }

  ruptura = {
    nombre: null,
    mensaje: null,
    usuario_creacion: null,
    lineaBase: null,
    proyecto: null,
    item: null  
  }
  itemTarget = null;

  openModalRuptura(target){
    this.itemTarget = target;
  }
  postRuptura(){
    if(this.ruptura.mensaje != null){
      this.ruptura.proyecto = Number(this.proyectoId);  
      this.ruptura.lineaBase = this.itemTarget.lineaBase.id;
      this.ruptura.usuario_creacion = this.user.id;
      this.ruptura.item = Number(this.itemTarget.id);
      this.apiSolicitud.post(this.ruptura).subscribe(
        data => {
          this.toastr.success("Solicitud enviada");
          this.getItems();
          $("#rupturaLBItem").modal("hide");
          console.log(this.ruptura);
        }
      )
       
    }else{
      this.toastr.error("Complete los campos obligatorios");
    }
  }
}
