import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoItemsRevisarComponent } from './proyecto-items-revisar.component';

describe('ProyectoItemsRevisarComponent', () => {
  let component: ProyectoItemsRevisarComponent;
  let fixture: ComponentFixture<ProyectoItemsRevisarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoItemsRevisarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoItemsRevisarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
