import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoNotificacionesComponent } from './proyecto-notificaciones.component';

describe('ProyectoNotificacionesComponent', () => {
  let component: ProyectoNotificacionesComponent;
  let fixture: ComponentFixture<ProyectoNotificacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoNotificacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoNotificacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
