import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SolicitudService } from 'src/app/_services/solicitud.service';

@Component({
  selector: 'app-proyecto-notificaciones',
  templateUrl: './proyecto-notificaciones.component.html',
  styleUrls: ['./proyecto-notificaciones.component.css']
})
export class ProyectoNotificacionesComponent implements OnInit {

  proyectoId = null;
  constructor(private apiNotificacion: SolicitudService, private route: ActivatedRoute, private datePipe: DatePipe) {
    this.route.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });
  }

  ngOnInit(): void {
    this.get();
  }

  notificaciones = [];

  get(){
    this.apiNotificacion.getNotificaciones(this.proyectoId).subscribe(
      data => {
        this.notificaciones = data;
        for(let i=0; i<this.notificaciones.length;i++){
          this.notificaciones[i].fecha_creacion = this.datePipe.transform(this.notificaciones[i].fecha_creacion, 'dd/MM/yyyy HH:mm:ss');
        }
      }
    )
  }
}
