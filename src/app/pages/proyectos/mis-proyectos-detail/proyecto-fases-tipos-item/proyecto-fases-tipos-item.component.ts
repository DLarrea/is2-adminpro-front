import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Fase } from 'src/app/_models/fase';
import { FaseService } from 'src/app/_services/fase.service';
import { TipoItemFaseService } from 'src/app/_services/tipoItemFase';
import { TipoItemService } from 'src/app/_services/tipoItem.service';

@Component({
  selector: 'app-proyecto-fases-tipos-item',
  templateUrl: './proyecto-fases-tipos-item.component.html',
  styleUrls: ['./proyecto-fases-tipos-item.component.css']
})
export class ProyectoFasesTiposItemComponent implements OnInit {

  proyectoId = null;
  faseId = null;
  tiposAsignados: any = [];
  tiposDisponibles: any = [];

  constructor(private route: ActivatedRoute, private toastr: ToastrService, 
    private apiFase: FaseService, private apiTipoItemFase: TipoItemFaseService, private apiTipoItem: TipoItemService) {
    this.route.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });

    this.route.paramMap.subscribe(params => {
      this.faseId = params.get("idFase");
    });
  }

  fase: Fase = new Fase();
  ngOnInit(): void {
    this.getFase();
    this.getTipoItemsAsignados();
    this.getTipoItemsDisponibles();
  }

  getFase() {
    this.apiFase.getById(this.faseId).subscribe(
      data => {
        this.fase = data;
      }
    )
  }


  getTipoItemsAsignados(){
    this.apiTipoItemFase.get(this.faseId).subscribe(
      data => {
        this.tiposAsignados = data;
        console.log(this.tiposAsignados);
      }
    )
  }

  postTiposItemFase(idTipoItem){
    this.apiTipoItemFase.post({fase: this.faseId, tipoitem: idTipoItem}).subscribe(
      data => {
        this.toastr.success("Tipo de item asignado");
        this.getTipoItemsAsignados();
      },
      error => {
        this.toastr.error("El tipo de item ya se encuentra asociado a la fase");
      }
    )
  }

  deleteTiposItemFase(id){
    this.apiTipoItemFase.delete(id).subscribe(
      data => {
        this.toastr.success("Tipo de item desasignado");
        this.getTipoItemsAsignados();
      }
    )
  }

  getTipoItemsDisponibles(){
    this.apiTipoItem.getByProyecto(this.proyectoId).subscribe(
      data => {
        this.tiposDisponibles = data;
        console.log(this.tiposDisponibles);
      }
    )
  }
}
