import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoFasesTiposItemComponent } from './proyecto-fases-tipos-item.component';

describe('ProyectoFasesTiposItemComponent', () => {
  let component: ProyectoFasesTiposItemComponent;
  let fixture: ComponentFixture<ProyectoFasesTiposItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoFasesTiposItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoFasesTiposItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
