import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoRelacionesComponent } from './proyecto-relaciones.component';

describe('ProyectoRelacionesComponent', () => {
  let component: ProyectoRelacionesComponent;
  let fixture: ComponentFixture<ProyectoRelacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoRelacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoRelacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
