import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RelacionService } from 'src/app/_services/relaciones.service';
import Hashids from 'hashids'
import { ToastrService } from 'ngx-toastr';
import { Node, Edge, ClusterNode } from '@swimlane/ngx-graph';
import { Subject } from 'rxjs';
import * as shape from 'd3-shape';

declare var $:any;
@Component({
  selector: 'app-proyecto-relaciones',
  templateUrl: './proyecto-relaciones.component.html',
  styleUrls: ['./proyecto-relaciones.component.css']
})
export class ProyectoRelacionesComponent implements OnInit {

  proyectoId= null;
  hashids = new Hashids('adminpro', 5)
  update$: Subject<boolean> = new Subject();
  curve = shape.curveMonotoneY;

  constructor(private route: ActivatedRoute, private apiRelacion: RelacionService, private toastr: ToastrService) {
    this.route.parent.params.subscribe(
      (params) => {
        this.proyectoId = params.id;
      });
  }

  colors = {
    "DE": "#007bff",
    "PP": "#ffc107",
    "AP": "#28a745",
    "CA": "#6c757d",
    "RE": "#dc3545"
  };

  ngOnInit(): void {
    this.getDatos();
    this.conexo();
  }
  p: number = 1;
  datosFases = [];
  datosRelaciones = [];
  itemsProyecto = [];
  itemsLoading = false;
  isconexo = null;
  addItemRelacion = {
    item_relacion_1: null,
    item_relacion_2: null
  }
  ei = {
    "DE": "DESARROLLO",
    "PP": "PENDIENTE DE APROBACIÓN",
    "AP": "APROBADO",
    "CA": "CANCELADO",
    "RE": "REVISIÓN"
  }

  eiColorInfo = [
    {
      nombre: "ITEMS EN DESARROLLO",
      color: "text-primary"
    },
    {
      nombre: "ITEMS APROBADOS",
      color: "text-success"
    },
    {
      nombre: "ITEMS PENDIENTES DE APROBACIÓN",
      color: "text-warning"
    },
    {
      nombre: "ITEMS CANCELADOS",
      color: "text-secondary"
    },{
      nombre: "ITEMS EN REVISIÓN",
      color: "text-danger"
    }
  ]
  eiColor = {
    "DE": "badge badge-primary",
    "PP": "badge badge-warning",
    "AP": "badge badge-success",
    "CA": "badge badge-secondary",
    "RE": "badge badge-danger"
  }

  conexo(){
    this.apiRelacion.esConexo(this.proyectoId).subscribe(
      data => {
        this.isconexo = data;
        console.log(data);
      }
    )
  }

  getDatos(){
    this.apiRelacion.get(this.proyectoId).subscribe(
      data => {
        this.datosFases = data.fases;
        this.datosRelaciones = data.relaciones;
        this.datosRelaciones.forEach(it => {
          it["codigo_1"] = this.hashids.encode(it.item_relacion_1);
          it["codigo_2"] = this.hashids.encode(it.item_relacion_2);
        });
        this.graficar();
        console.log(this.datosFases, this.datosRelaciones);
      }
    )
  }

  getItemsProyecto(){
    this.addItemRelacion.item_relacion_1 = null;
    this.addItemRelacion.item_relacion_2 = null;
    this.itemsProyecto = [];
    this.itemsLoading = true;
    this.apiRelacion.getItemsProyecto(this.proyectoId).subscribe(
      data => {
        this.itemsProyecto = data;
        console.log(this.itemsProyecto);
        this.itemsLoading = false;
        this.itemsProyecto.forEach(it => {
          it["codigo"] = this.hashids.encode(it.id);
        });
      }
    )
  }

  post(){
    this.apiRelacion.post(this.addItemRelacion).subscribe(
      data => {
        this.toastr.success("La relación fue guardada");
        $("#agregarRelacion").modal("hide");
        this.addItemRelacion.item_relacion_1 = null;
        this.addItemRelacion.item_relacion_2 = null;  
        this.getDatos();
        this.conexo();
      },
      error => {
        this.toastr.error(error.error.mensaje,"ERROR")
        console.log(error);
      }
    )
  }

  deleteRelacion = null;
  openDelete(id){
    this.deleteRelacion = Number(id);
  }

  delete(){
    this.apiRelacion.delete(this.deleteRelacion).subscribe(
      data => {
        this.toastr.success("Relación eliminada");
        this.getDatos();
        this.conexo();
      }
    )
  }

  graficar(){
    this.nodos = [];
    this.aristas = [];
    this.clusters = [];
    for(let i=0; i<this.datosFases.length;i++){
      let cluster = this.datosFases[i];
      for(let j=0; j<cluster.items.length;j++){
        let item = cluster.items[j];
        console.log(item);
        this.nodos.push(
          {
            id: `it-${item.id.toString()}`,
            label: this.hashids.encode(item.id.toString()),
            data: {
              estado_color: this.colors[item.estado.nombre],
              lb: item.lineaBase != null? item.lineaBase.codigo: "",
              nombre_item: item.nombre
            }
          } 
        )
      }
    }

    for(let i=0; i<this.datosFases.length;i++){
      let items = [];
      let cluster = this.datosFases[i];
      if(cluster.items.length > 0){
        for(let j=0; j<cluster.items.length;j++){
          let item = cluster.items[j];
          items.push(`it-${item.id.toString()}`);
        }
        this.clusters.push(
          {
            id: `fase-${i}`,
            label: cluster.fase_nombre,
            childNodeIds: items
          } 
        )
      }
    }

    for(let i=0; i<this.datosRelaciones.length;i++){
      let rel = this.datosRelaciones[i];
      this.aristas.push(
        {
          id: `rel-${rel.id.toString()}`,
          source: `it-${rel.item_relacion_1.toString()}`,
          target: `it-${rel.item_relacion_2.toString()}`,
          label: rel.tipo
        } 
      )
    }

    console.log(this.nodos, this.aristas, this.clusters);
    this.updateGraph();
  }

  nodos:Node[] = [];
  clusters: ClusterNode[] = [];
  aristas:Edge[] = [];

  updateGraph() {
    this.update$.next(true)
  }
}
