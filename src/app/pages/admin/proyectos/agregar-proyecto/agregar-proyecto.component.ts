import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/_services/user.service';
import { Proyecto } from 'src/app/_models/proyecto';
import { ProyectoService } from 'src/app/_services/proyecto.service'
import { ToastrService } from 'ngx-toastr';
declare var $:any;
@Component({
  selector: 'app-agregar-proyecto',
  templateUrl: './agregar-proyecto.component.html',
  styleUrls: ['./agregar-proyecto.component.css']
})
export class AgregarProyectoComponent implements OnInit {

  constructor(private apiUser: UserService, private apiProyecto: ProyectoService, private toastr: ToastrService) { }

  usuarios = [];

  proyecto: Proyecto = new Proyecto();
  user = null;
  ngOnInit(): void {
    this.getUsers();
  }

  getUsers(){
    this.apiUser.get().subscribe(
      data => {
        this.usuarios = data;
        setTimeout(() => {
          $("#userSelect").selectpicker();
        }, 0);
      }
    )
  }

  postProyecto(){
    if(this.proyecto.descripcion != null && this.proyecto.nombre  != null && this.proyecto.gerente_id != null
      && this.proyecto.cantidad_comite != null && this.proyecto.cantidad_comite % 2 != 0 && this.proyecto.cantidad_comite >= 3){
      
      this.apiProyecto.post(this.proyecto).subscribe(
        data => {
          this.toastr.success("Proyecto creado")
          this.proyecto = new Proyecto();
          this.user = null;
        },
        error => {
          this.toastr.error("Error al crear proyecto");
        }
      )
    }else {
      this.toastr.error("Complete los campos y/o error en los campos");
    }
    
  }
}
