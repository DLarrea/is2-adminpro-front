
export class LineaBase {
    id: number = null;
    nombre: string = null;
    descripcion: string = null;
    observacion: string = null;
    codigo: string = null;
    estado: any = null;
    fase: any = null;
}