export class TipoItem {
    id: number = null;
    nombre: string = null;
    descripcion: string = null;
    fase: string = null;
    proyecto: string = null;
}