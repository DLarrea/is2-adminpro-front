export class Fase {
    id: number = null;
    nombre: string = null;
    descripcion: string = null;
    orden: number = null;
    estado_fase: string = null;
    proyecto: number = null;
}