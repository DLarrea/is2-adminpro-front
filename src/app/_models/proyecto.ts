export class Proyecto {
    id: number = null;
    nombre: string = null;
    descripcion: string = null;
    estadoProyecto: any = null;
    gerente_id: number = null;
    fecha_inicio: string = null;
    observacion: string = null;
    cantidad_comite: number = null;
  }
  