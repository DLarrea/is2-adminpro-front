export class Item {
    id: number = null;
    nombre: string = null;
    descripcion: string = null;
    version: number = null;
    prioridad: number = null;
    lineaBase: any = null;
    estado: any = null;
    tipo: any = null;
    ant: any = null;
    sig: any = null;
    current: boolean = null;
    values: string = null;
    estado_ant: any = null;
}
