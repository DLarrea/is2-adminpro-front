import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";
import { map, catchError } from "rxjs/operators";

import { User } from "../_models/user";
import { AppSettings } from "./app.config";

@Injectable({ providedIn: "root" })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<Object>;
  public currentUser: Observable<Object>;
  private tokenSubject: BehaviorSubject<Object>;
  public token: Observable<Object>;

  constructor(private http: HttpClient) {}

  public localStorage() {
    this.currentUserSubject = new BehaviorSubject<Object>(
      JSON.parse(sessionStorage.getItem("currentUser"))
    );
    this.currentUser = this.currentUserSubject.asObservable();

    this.tokenSubject = new BehaviorSubject<Object>(
      sessionStorage.getItem("token")
    );
    this.token = this.tokenSubject.asObservable();
  }

  public get currentUserValue(): Object {
    return this.currentUserSubject.value;
  }

  public get tokenValue(): Object {
    return this.tokenSubject.value;
  }

  login(correoElectronico: string, password: string): Observable<any> {
    const body = { username: correoElectronico, password: password };
    return this.http.post(`${AppSettings.api}/login/`, body);
  }

  logout() {
    sessionStorage.removeItem("currentUser");
    this.currentUserSubject.next(null);
    sessionStorage.removeItem("token");
    this.tokenSubject.next(null);
  }

  saveData(user, token) {
    sessionStorage.setItem("currentUser", JSON.stringify(user));
    sessionStorage.setItem("token", JSON.stringify(token));
  }
}
