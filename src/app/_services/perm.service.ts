import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AppSettings } from "./app.config";

@Injectable({
  providedIn: "root"
})
export class PermService {
  constructor(private http: HttpClient) {}

  getPermisosSistema(idUser): Observable<any> {
    return this.http.get(`${AppSettings.api}/user/${idUser}/permisos-sistema`);
  }

  getPermisosProyecto(idUser, idProyecto): Observable<any> {
    return this.http.get(`${AppSettings.api}/user/${idUser}/permisos-proyecto/${idProyecto}`);
  }

  getPermisosProyectoFase(idUser, idProyecto, idFase): Observable<any> {
    return this.http.get(`${AppSettings.api}/user/${idUser}/permisos-proyecto/${idProyecto}/fase/${idFase}`);
  }

}