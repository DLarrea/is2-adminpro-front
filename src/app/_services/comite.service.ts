import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AppSettings } from "./app.config";

@Injectable({
  providedIn: "root"
})
export class ComiteService {
  constructor(private http: HttpClient) {}

  get(idProyecto): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append("proyecto", idProyecto.toString());
    return this.http.get(`${AppSettings.api}/comite/`, {params: httpParams});
  }

  isMiembro(idProyecto, idUser): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append("proyecto", idProyecto.toString());
    return this.http.get(`${AppSettings.api}/iscomite/${idUser}`, {params: httpParams});
  }

  post(body): Observable<any> {
    return this.http.post(`${AppSettings.api}/comite/`, body);
  }

  put(body): Observable<any> {
    return this.http.put(`${AppSettings.api}/comite/${body.id}`, body);
  }

  delete(idComite): Observable<any> {
    return this.http.delete(`${AppSettings.api}/comite/${idComite}`);
  }
}