import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AppSettings } from "./app.config";

@Injectable({
  providedIn: "root"
})
export class AtributoService {
  constructor(private http: HttpClient) {}

  get(): Observable<any> {
    return this.http.get(`${AppSettings.api}/atributo/`);
  }

  getById(idAtributo): Observable<any> {
    return this.http.get(`${AppSettings.api}/atributo/${idAtributo}`);
  }

  post(body): Observable<any> {
    return this.http.post(`${AppSettings.api}/atributo/`, body);
  }

  delete(idAtributo): Observable<any> {
    return this.http.delete(`${AppSettings.api}/atributo/${idAtributo}`, );
  }
}