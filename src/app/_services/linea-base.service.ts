import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AppSettings } from "./app.config";

@Injectable({
  providedIn: "root"
})
export class LineaBaseService {
  constructor(private http: HttpClient) {}

  get(idFase): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append("fase", idFase.toString());
    return this.http.get(`${AppSettings.api}/linea-base/`, {params: httpParams});
  }

  getById(idLB): Observable<any> {
    return this.http.get(`${AppSettings.api}/linea-base/${idLB}`);
  }

  put(body): Observable<any> {
    return this.http.put(`${AppSettings.api}/linea-base/${body.id}`, body);
  }

  post(body): Observable<any> {
    return this.http.post(`${AppSettings.api}/linea-base/`, body);
  }

  delete(idLB): Observable<any> {
    return this.http.delete(`${AppSettings.api}/linea-base/${idLB}`, );
  }

  cerrarLB(id, body): Observable<any>{
    return this.http.put(`${AppSettings.api}/linea-base/cerrar/${id}`, body)
  }
}
