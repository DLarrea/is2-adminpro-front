import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AppSettings } from "./app.config";

@Injectable({
  providedIn: "root"
})
export class EstadoLineaBaseService {
  constructor(private http: HttpClient) {}

  getEstados(): Observable<any> {
    return this.http.get(`${AppSettings.api}/linea-base/estados`);
  }

}