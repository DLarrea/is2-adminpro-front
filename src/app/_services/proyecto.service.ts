import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AppSettings } from "./app.config";
import { User } from "../_models/user";

@Injectable({
  providedIn: "root"
})
export class ProyectoService {
  constructor(private http: HttpClient) {}

  get(): Observable<any> {
    return this.http.get(`${AppSettings.api}/proyecto/`);
  }

  post(body): Observable<any> {
    return this.http.post(`${AppSettings.api}/proyecto/`, body);
  }

  put(body): Observable<any> {
    return this.http.put(`${AppSettings.api}/proyecto/${body.id}`, body);
  }

  getByUserId(idUser): Observable<any> {
    return this.http.get(`${AppSettings.api}/proyecto/user/${idUser}`);
  }

  getById(id): Observable<any>{
    return this.http.get(`${AppSettings.api}/proyecto/${id}`);
  }

  getUsers(id): Observable<any> {
    return this.http.get(`${AppSettings.api}/proyecto/${id}/users`);
  }

  agergarUsuario(body): Observable<any> {
    return this.http.post(`${AppSettings.api}/proyecto/user`, body);
  }

  iniciarProyecto(body): Observable<any> {
    return this.http.put(`${AppSettings.api}/proyecto/${body.id}/iniciar`, body);
  }

  rolesUsuarioProyecto(userId, proyectoId): Observable<any> {
    return this.http.get(`${AppSettings.api}/user/${userId}/roles-asignados-proyecto/${proyectoId}`);
  }

}