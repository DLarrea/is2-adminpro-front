import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AppSettings } from "./app.config";
import { User } from "../_models/user";

@Injectable({
  providedIn: "root"
})
export class UserService {
  constructor(private http: HttpClient) {}

  post(body): Observable<any> {
    return this.http.post(`${AppSettings.api}/user/`, body);
  }

  get(): Observable<any> {
    return this.http.get(`${AppSettings.api}/user/`);
  }

  put(user: User): Observable<any> {
    return this.http.put(`${AppSettings.api}/user/${user.id}`, user);
  }

  postUserRolSistema(idUser,idRol): Observable<any> {
    return this.http.post(`${AppSettings.api}/user/${idUser}/roles-sistema/${idRol}`, {})
  }

  deleteUserRolSistema(idUser,idRol): Observable<any> {
    return this.http.delete(`${AppSettings.api}/user/${idUser}/roles-sistema/${idRol}`, {})
  }

  postUserRolProyecto(idUser,idRol): Observable<any> {
    return this.http.post(`${AppSettings.api}/user/${idUser}/roles-proyecto/${idRol}`, {})
  }

  deleteUserRolProyecto(idUser,idProyecto): Observable<any> {
    return this.http.delete(`${AppSettings.api}/user/${idUser}/roles-proyecto/${idProyecto}`, {})
  }

  deleteUserProyecto(idUser, idProyecto): Observable<any> {
    return this.http.delete(`${AppSettings.api}/user/${idUser}/proyecto/${idProyecto}/eliminar`, {})
  }
}
