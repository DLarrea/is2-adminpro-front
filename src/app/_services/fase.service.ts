import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AppSettings } from "./app.config";

@Injectable({
  providedIn: "root"
})
export class FaseService {
  constructor(private http: HttpClient) {}

  get(idProyecto): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append("proyecto", idProyecto.toString());
    return this.http.get(`${AppSettings.api}/fase/`, {params: httpParams});
  }

  getById(idFase): Observable<any> {
    return this.http.get(`${AppSettings.api}/fase/${idFase}`);
  }

  put(body): Observable<any> {
    return this.http.put(`${AppSettings.api}/fase/${body.id}`, body);
  }

  post(body): Observable<any> {
    return this.http.post(`${AppSettings.api}/fase/`, body);
  }

  delete(idFase): Observable<any> {
    return this.http.delete(`${AppSettings.api}/fase/${idFase}`, );
  }

  iniciarFase(id): Observable<any>{
    return this.http.put(`${AppSettings.api}/fase/${id}/iniciar`, {})
  }
}
