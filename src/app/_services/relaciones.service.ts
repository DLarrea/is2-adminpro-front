import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AppSettings } from "./app.config";

@Injectable({
  providedIn: "root"
})
export class RelacionService {
  constructor(private http: HttpClient) {}

  get(proyectoId): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append("proyecto", proyectoId.toString());
    return this.http.get(`${AppSettings.api}/relacionar-item/`, {params:httpParams});
  }

  post(body): Observable<any> {
    return this.http.post(`${AppSettings.api}/relacionar-item/`, body);
  }

  delete(relId): Observable<any> {
    return this.http.delete(`${AppSettings.api}/relacionar-item/${relId}`);
  }

  esConexo(proyectoId): Observable<any> {
    return this.http.get(`${AppSettings.api}/relacionar-item/${proyectoId}`);
  }

  getItemsProyecto(proyectoId): Observable<any> {
    return this.http.get(`${AppSettings.api}/item-proyecto/${proyectoId}`);
  }
}