import { Injectable } from "@angular/core";
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: "root"
})
export class FirebaseService {
  constructor(private firestore: AngularFirestore) {}

  get(){
    return this.firestore.collection("archivos").snapshotChanges();
  }

  post(archivo){
    return this.firestore.collection("archivos").add(archivo);
  }

  delete(id){
    return this.firestore.collection("archivos").doc(id).delete();
  }
}
