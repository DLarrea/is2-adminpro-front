import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AppSettings } from "./app.config";

@Injectable({
  providedIn: "root"
})
export class EstadoItemService {
  constructor(private http: HttpClient) {}

  getEstados(): Observable<any> {
    return this.http.get(`${AppSettings.api}/item-estado/`);
  }

  getById(id): Observable<any> {
    return this.http.get(`${AppSettings.api}/item-estado/${id}`);
  }
}