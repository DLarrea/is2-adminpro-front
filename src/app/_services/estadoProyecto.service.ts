import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AppSettings } from './app.config';


@Injectable({
  providedIn: "root"
})
export class EstadoProyectoService {
  constructor(private http: HttpClient) {}

  get(): Observable<any> {
    return this.http.get(`${AppSettings.api}/estado-proyecto/`);
  }

}