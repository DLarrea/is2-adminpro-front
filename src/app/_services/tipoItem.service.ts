import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AppSettings } from "./app.config";
import { TipoItem } from '../_models/tipoItem';

@Injectable({
  providedIn: "root"
})
export class TipoItemService {
  constructor(private http: HttpClient) {}

  get(idFase): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append("fase", idFase.toString());
    return this.http.get(`${AppSettings.api}/tipoItem/fases`, {params: httpParams});
  }

  getByProyecto(proyecto): Observable<any> {
    return this.http.get(`${AppSettings.api}/tipoItem/proyecto/${proyecto}`);
  }

  getById(idTipoItem): Observable<any> {
    return this.http.get(`${AppSettings.api}/tipoItem/${idTipoItem}`);
  }

  post(body): Observable<any> {
    return this.http.post(`${AppSettings.api}/tipoItem/`, body);
  }
  
  put(tipoItem: TipoItem): Observable<any> {
    return this.http.put(`${AppSettings.api}/tipoItem/${tipoItem.id}`, tipoItem);
  }

  delete(idTipoItem): Observable<any> {
    return this.http.delete(`${AppSettings.api}/tipoItem/${idTipoItem}` );
  }

  gestionar(idTipoItem): Observable<any> {
    return this.http.get(`${AppSettings.api}/tipoItem/${idTipoItem}/gestionar`);
  }
  
  gestionarAsignar(idTipoItem, idAtributo): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append("atributo", idAtributo.toString());
    return this.http.post(`${AppSettings.api}/tipoItem/${idTipoItem}/gestionar`, {}, {params: httpParams});
  }

  gestionarDesasignar(idTipoItem, idAtributo): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append("atributo", idAtributo.toString());
    return this.http.delete(`${AppSettings.api}/tipoItem/${idTipoItem}/gestionar`, {params: httpParams});
  }

  getImportables(idProyecto): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append("proyecto", idProyecto.toString());
    return this.http.get(`${AppSettings.api}/tipoItem/importar`, {params: httpParams});
  }

  postByImport(body): Observable<any> {
    return this.http.post(`${AppSettings.api}/tipoItem/importar`, body);
  }

  getTipoItemAtributos(idTipoItem): Observable<any> {
    return this.http.get(`${AppSettings.api}/tipoItem/${idTipoItem}/atributos`);
  }
}
