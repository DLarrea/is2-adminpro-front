import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AppSettings } from "./app.config";
import { TipoItem } from '../_models/tipoItem';

@Injectable({
  providedIn: "root"
})
export class SolicitudService {
  constructor(private http: HttpClient) {}

  get(idProyecto): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append("proyecto", idProyecto.toString());
    return this.http.get(`${AppSettings.api}/solicitud/`, {params: httpParams});
  }

  post(body): Observable<any> {
    return this.http.post(`${AppSettings.api}/solicitud/`, body);
  }

  put(body): Observable<any> {
    return this.http.put(`${AppSettings.api}/solicitud/${body.solicitud}`, body);
  }

  getNotificaciones(idProyecto): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append("proyecto", idProyecto.toString());
    return this.http.get(`${AppSettings.api}/notificacion/`, {params: httpParams});
  }
}
