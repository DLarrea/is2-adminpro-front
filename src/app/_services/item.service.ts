import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AppSettings } from "./app.config";

@Injectable({
  providedIn: "root"
})
export class ItemService {
  constructor(private http: HttpClient) {}

  get(faseId): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append("fase", faseId.toString());
    return this.http.get(`${AppSettings.api}/item/`, {params:httpParams});
  }

  getByLB(faseId, lbId): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append("fase", faseId.toString());
    httpParams = httpParams.append("lb", lbId.toString());
    return this.http.get(`${AppSettings.api}/item/`, {params:httpParams});
  }

  getById(itemId): Observable<any> {
    return this.http.get(`${AppSettings.api}/item/${itemId}`);
  }

  getImpacto(itemId): Observable<any> {
    return this.http.get(`${AppSettings.api}/item-impacto/${itemId}`);
  }

  getHistorial(itemId): Observable<any> {
    return this.http.get(`${AppSettings.api}/item/${itemId}/historial`);
  }

  post(body): Observable<any> {
    return this.http.post(`${AppSettings.api}/item/`, body);
  }

  put(body): Observable<any> {
    return this.http.put(`${AppSettings.api}/item/${body.id}`, body);
  }

  postHistorial(itemId, body): Observable<any> {
    return this.http.post(`${AppSettings.api}/item/${itemId}/historial`, body);
  }

  getItemsRevision(proyectoId): Observable<any> {
    return this.http.get(`${AppSettings.api}/item-revision/${proyectoId}`);
  }
}