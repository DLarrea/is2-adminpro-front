import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AppSettings } from './app.config';

@Injectable({
  providedIn: "root"
})
export class TipoItemFaseService {
  constructor(private http: HttpClient) {}

  get(idFase): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append("fase", idFase.toString());
    return this.http.get(`${AppSettings.api}/tipoItem/fases/`, {params: httpParams});
  }

  getById(id): Observable<any> {
    return this.http.get(`${AppSettings.api}/tipoItem/fases/${id}`);
  }

  post(body): Observable<any> {
    return this.http.post(`${AppSettings.api}/tipoItem/fases/`, body);
  }

  delete(id): Observable<any> {
    return this.http.delete(`${AppSettings.api}/tipoItem/fases/${id}`, );
  }
}